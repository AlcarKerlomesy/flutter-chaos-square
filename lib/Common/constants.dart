// UI
const double topBarHeight = 100 ; // in pixels
const double sideBarWidth = 100 ; // in pixels
const double sideBarHeightRatio = 0.6 ; // relative to screen height
const double barElementAlign = 0.8 ;

const double widthButton = 200 ; // in pixels

const double maxPlayableSize = 800 ; // in pixels
const double boardSizeRatio = 5.0/8.0 ; // relative to playable size

const double numberFontSize = 0.5 ; // relative to tile size
const double numberStrokeWidth = 3.0/50.0 ; // relative to tile size

const double sizeRedPointer = 0.06 ; // relative to board size

// List lengths
const int numTilePosition = 16 ; // 16 positions
const int listTileLength = 16 ; // ghost + 15 tiles
const int listPieceLength = 45 ; // 3 per tiles, ghost excluded

// Circle
const double circleSize = 0.25*0.8 ; // relative tp board size
const double circleOpacity = 0.3 ;

// Foe
const double spawnLine = 1.5 ; // Relative to the board sizeconds

const int defaultFadeIn = 1500 ; // in milliseconds
const int defaultFadeOut = 300 ; // in milliseconds

const double defaultVelocity = 2 ; // in tiles per second
const double defaultMaxAngleVariation = 120 ; // in degrees per second

const double defaultActorSize = 0.1 ; // relative to board size
const double defaultActorSizeDiff = 0.2 ; // relative to board size