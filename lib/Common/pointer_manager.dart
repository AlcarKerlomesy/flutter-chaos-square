import 'package:flutter/material.dart' ;
import 'package:vector_math/vector_math_64.dart' ;
import 'coordinate_transform.dart';
import 'constants.dart' as cst;
import 'callback_typedef.dart';
import '../GameMechanics/PuzzleMechanics/puzzle.dart';

class PointerManager {
  static bool playableIsSet = false ;
  static Vector2 playableOffset = Vector2(0, 0) ;
  static Vector2 playableSize = Vector2(0, 0) ;
  static Callback? updatePlayableSize ;

  static bool boardIsSet = false ;
  static Vector2 boardOffset = Vector2(0, 0) ;
  static Vector2 boardSize = Vector2(0, 0) ;
  static Callback? updateBoardSize ;

  static bool isOutOfBound = false ;
  static Vector2 pointerPosition = Vector2(0, 0) ;
  static Callback? redrawRedPointer ;

  static bool blockPointerCallback = false ;

  static setLayoutParameters(RenderBox box, bool isBoard) {
    Offset offset = box.localToGlobal(Offset.zero) ;

    if (isBoard) {
      boardOffset = Vector2(offset.dx, offset.dy) ;
      boardSize = Vector2(box.constraints.maxWidth, box.constraints.maxHeight) ;
      boardIsSet = true ;
    } else {
      playableOffset = Vector2(offset.dx, offset.dy) ;
      playableSize = Vector2(box.constraints.maxWidth, box.constraints.maxHeight) ;
      playableIsSet = true ;
    }
  }

  static Vector2 getGlobalPointerPosition(PointerEvent details) {
    return Vector2(details.position.dx, details.position.dy) ;
  }

  static Vector2 getPuzzlePosition(PointerEvent details) {
    return pointerToPuzzlePosition(
        getGlobalPointerPosition(details),
        boardOffset, boardSize
    ) ;
  }

  static Vector2 getRedPointerPosition(PointerEvent details) {
    return pointerToUiPosition(
        getGlobalPointerPosition(details),
        playableOffset, playableSize, cst.sizeRedPointer
    ) ;
  }

  static pointerDownOutOfBound(PointerEvent details) {
    if (!blockPointerCallback) {
      if (!boardIsSet ||
          !Puzzle.pointerDownBrokenPiece(getPuzzlePosition(details))
      ) {
        isOutOfBound = true ;
        pointerMoveOutOfBound(details) ;
      }

      PointerManager.blockPointerCallback = true ;
    }
  }

  static pointerDown(PointerEvent details) {
    if (!blockPointerCallback && boardIsSet) {
      Puzzle.pointerDown(getPuzzlePosition(details)) ;

      PointerManager.blockPointerCallback = true ;
    }
  }

  static pointerMoveOutOfBound(PointerEvent details) {
    if (!blockPointerCallback) {
      if (Puzzle.selectedPiece == -1) {
        if (playableIsSet) {
          pointerPosition = getRedPointerPosition(details) ;
          redrawRedPointer?.call();
        }
      } else {
        Puzzle.pointerMoveBrokenPiece(getPuzzlePosition(details)) ;
      }

      blockPointerCallback = true ;
    }
  }

  static pointerMove(PointerEvent details) {
    if (!blockPointerCallback) {
      bool oldIsOutOfBound = isOutOfBound ;
      isOutOfBound = Puzzle.pointerMove(
          getPuzzlePosition(details)) ;

      if (isOutOfBound) {
        pointerMoveOutOfBound(details) ;
      } else if (oldIsOutOfBound == true) {
        redrawRedPointer?.call() ;
      }

      blockPointerCallback = true ;
    }
  }

  static pointerUpOutOfBound() {
    if (!blockPointerCallback) {
      if (Puzzle.selectedPiece == -1) {
        isOutOfBound = false;
        redrawRedPointer?.call();
      } else {
        Puzzle.pointerUpBrokenPiece() ;
      }

      blockPointerCallback = true ;
    }
  }

  static pointerUp() {
    if (!blockPointerCallback) {
      Puzzle.pointerUp();

      if (isOutOfBound) {
        pointerUpOutOfBound() ;
      }

      blockPointerCallback = true ;
    }
  }
}