import 'package:vector_math/vector_math_64.dart' ;

// Puzzle coordinate:
//    top-left: (-0.5, -0.5)
//    bottom-right: (3.5, 3.5)

// UI coordinate:
//    top-left: (-1, -1)
//    bottom-right: (1, 1)

// ("Global pointer position" - "W. offset")/"W. size":
//    top-left: (0, 0)
//    bottom-right: (1, 1)

Vector2 puzzleToUIPosition(Vector2 position, [double widgetSize = 0]) {
  return adaptUiPosition(position/2 - Vector2(0.75, 0.75), widgetSize) ;
}

Vector2 uiToPuzzlePosition(Vector2 position) {
  return position*2 + Vector2(1.5, 1.5) ;
}

Vector2 adaptUiPosition(Vector2 position, double elementSize) {
  return position/(1 - elementSize) ;
}

Vector2 pointerToUiPosition(
    Vector2 position, Vector2 widgetOffset,
    Vector2 widgetSize, [double elementSize = 0]
) {
  position..sub(widgetOffset)
      ..divide(widgetSize) ;

  return adaptUiPosition(position*2 - Vector2(1, 1), elementSize) ;
}

Vector2 pointerToPuzzlePosition(
    Vector2 position, Vector2 widgetOffset, Vector2 widgetSize
) {
  position..sub(widgetOffset)
    ..divide(widgetSize) ;

  return position*4 - Vector2(0.5, 0.5) ;
}