import 'package:flutter/material.dart';
import 'UserInterface/root_ui.dart' ;
import 'GameMechanics/game_master.dart';

void main() {
  GameMaster.init() ;
  runApp(const MyApp());
}