import 'package:flutter/material.dart' ;
import '../GameMechanics/counters.dart' ;

class UpdateNumMoves extends ChangeNotifier {
  static int numMoves = Counters.numMoves ;

  UpdateNumMoves():super() {
    Counters.redrawMoves = updateNumMoves ;
  }

  @override
  void dispose() {
    if (Counters.redrawMoves == updateNumMoves) {
      Counters.redrawMoves = null ;
    }

    super.dispose();
  }

  updateNumMoves() {
    numMoves = Counters.numMoves ;
    notifyListeners() ;
  }
}

class UpdateTimer extends ChangeNotifier {
  static String timerDisplay = getTimerDisplay(Counters.curTime) ;
  static bool isTimerVisible = Counters.isTimerVisible ;

  UpdateTimer():super() {
    Counters.redrawTimer = updateNumMoves ;
  }

  @override
  void dispose() {
    if (Counters.redrawTimer == updateNumMoves) {
      Counters.redrawTimer = null ;
    }

    super.dispose();
  }

  updateNumMoves() {
    timerDisplay = getTimerDisplay(Counters.curTime) ;
    isTimerVisible = Counters.isTimerVisible ;
    notifyListeners() ;
  }

  static String getTimerDisplay(Duration timer) {
    int seconds = timer.inSeconds ;

    if (seconds < 0) {
      return '0:00' ;
    } else {
      seconds %= 60 ;
      return timer.inMinutes.toString()
          + (seconds < 10 ? ':0' : ':')
          + seconds.toString() ;
    }
  }
}