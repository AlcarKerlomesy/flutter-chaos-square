import 'package:flutter/material.dart' ;
import '../GameMechanics/Foe/foe_master.dart' ;

class UpdateBackground extends ChangeNotifier {
  static bool isDanger = FoeMaster.foesIsAttacking ;

  UpdateBackground() : super() {
    FoeMaster.redrawBackground = updateBackground ;
  }

  @override
  void dispose() {
    if (FoeMaster.redrawBackground == updateBackground) {
      FoeMaster.redrawBackground = null ;
    }

    super.dispose();
  }

  updateBackground() {
    isDanger = FoeMaster.foesIsAttacking ;
    notifyListeners() ;
  }
}