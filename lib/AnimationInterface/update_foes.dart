import 'package:flutter/material.dart' ;
import '../../GameMechanics/Foe/foe_master.dart' ;
import '../../GameMechanics/Foe/foe_actor.dart' ;

export '../../GameMechanics/Foe/foe_actor.dart' ;

class UpdateFoes extends ChangeNotifier {
  static int listFoesLength = FoeMaster.listFoes.length ;

  UpdateFoes():super() {
    FoeMaster.redrawFoes = updateFoes ;
  }

  @override
  void dispose() {
    if (FoeMaster.redrawFoes == updateFoes) {
      FoeMaster.redrawFoes = null ;
    }

    super.dispose();
  }

  static FoeActor getFoe(int index) {
    return FoeMaster.listFoes[index] ;
  }

  updateFoes() {
    listFoesLength = FoeMaster.listFoes.length ;
    notifyListeners() ;
  }
}