import 'package:flutter/material.dart' ;
import '../GameMechanics/game_master.dart' ;

enum MenuSelect {
  none,
  startMenu,
  pauseMenu,
  winMenu,
  loseMenu,
}

class UpdateMenu extends ChangeNotifier {
  static MenuSelect menuSelect = GameMaster.menuSelect ;

  UpdateMenu():super() {
    GameMaster.menuRedraw = updateMenu ;
  }

  @override
  void dispose() {
    if (GameMaster.menuRedraw == updateMenu) {
      GameMaster.menuRedraw = null ;
    }

    super.dispose();
  }

  updateMenu() {
    menuSelect = GameMaster.menuSelect ;
    notifyListeners() ;
  }
}