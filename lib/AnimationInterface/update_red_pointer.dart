import 'package:flutter/material.dart' ;
import 'package:vector_math/vector_math_64.dart' show Vector2 ;
import '../Common/pointer_manager.dart' ;

class UpdateRedPointer extends ChangeNotifier {
  static Vector2 pointerPosition = PointerManager.pointerPosition ;
  static bool isOutOfBound = PointerManager.isOutOfBound ;

  UpdateRedPointer():super() {
    PointerManager.redrawRedPointer = updatePointerPosition ;
  }

  @override
  void dispose() {
    if (PointerManager.redrawRedPointer == updatePointerPosition) {
      PointerManager.redrawRedPointer = null ;
    }

    super.dispose();
  }

  updatePointerPosition() {
    pointerPosition = PointerManager.pointerPosition ;
    isOutOfBound = PointerManager.isOutOfBound ;
    notifyListeners() ;
  }
}