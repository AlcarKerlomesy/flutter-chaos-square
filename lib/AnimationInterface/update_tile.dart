import 'package:flutter/material.dart' ;
import '../GameMechanics/PuzzleMechanics/tile_state.dart' ;
import '../GameMechanics/PuzzleMechanics/puzzle.dart' ;

export '../GameMechanics/PuzzleMechanics/tile_state.dart' show TileState ;

class UpdateTileVisibility extends ChangeNotifier {
  late final TileState tileState ;
  static bool isSelected = Puzzle.isSelected ;

  UpdateTileVisibility(this.tileState):super() {
    tileState.redrawTileVisibility = updateTileVisibility ;
  }

  @override
  void dispose() {
    if (tileState.redrawTileVisibility == updateTileVisibility) {
      tileState.redrawTileVisibility = null ;
    }

    super.dispose();
  }

  updateTileVisibility() {
    isSelected = Puzzle.isSelected ;
    notifyListeners() ;
  }
}

class UpdateTilePosition extends ChangeNotifier {
  late final TileState tileState ;

  UpdateTilePosition(this.tileState):super() {
    tileState.redrawTilePosition = notifyListeners ;
  }

  @override
  void dispose() {
    if (tileState.redrawTilePosition == notifyListeners) {
      tileState.redrawTilePosition = null ;
    }

    super.dispose();
  }
}

class UpdateTileState extends ChangeNotifier {
  late final TileState tileState ;

  UpdateTileState(this.tileState):super() {
    tileState.redrawTileState = notifyListeners ;
  }

  @override
  void dispose() {
    if (tileState.redrawTileState == notifyListeners) {
      tileState.redrawTileState = null ;
    }

    super.dispose();
  }
}