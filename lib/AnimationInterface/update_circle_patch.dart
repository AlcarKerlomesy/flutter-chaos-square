import 'package:flutter/material.dart' ;
import 'package:chaos_square/GameMechanics/PuzzleMechanics/circle_state.dart';
import '../GameMechanics/PuzzleMechanics/puzzle.dart' ;

export '../GameMechanics/PuzzleMechanics/circle_state.dart' show CircleState ;

class UpdateCirclePatch extends ChangeNotifier {
  static int circleListLength = 0 ;
  static CircleState Function(int) getCircleState = Puzzle.getCircleState ;

  UpdateCirclePatch():super() {
    Puzzle.redrawCirclePatch = updateCircleListLength ;
  }

  @override
  void dispose() {
    if (Puzzle.redrawCirclePatch == updateCircleListLength) {
      Puzzle.redrawCirclePatch = null ;
    }

    super.dispose();
  }

  updateCircleListLength() {
    circleListLength = Puzzle.secretCircles.length ;
    notifyListeners() ;
  }
}