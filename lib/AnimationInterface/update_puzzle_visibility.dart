import 'package:flutter/material.dart' ;
import '../GameMechanics/PuzzleMechanics/puzzle.dart' ;

class UpdatePuzzleVisibility extends ChangeNotifier {
  static bool hidePuzzle = Puzzle.isHidden;

  UpdatePuzzleVisibility():super() {
      Puzzle.redrawPuzzleVisibility = redrawPuzzleVisibility ;
  }

  @override
  void dispose() {
    if (Puzzle.redrawPuzzleVisibility == redrawPuzzleVisibility) {
      Puzzle.redrawPuzzleVisibility = null ;
    }

    super.dispose();
  }

  redrawPuzzleVisibility() {
    hidePuzzle = Puzzle.isHidden ;
    notifyListeners() ;
  }
}