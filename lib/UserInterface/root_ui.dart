import 'dart:async' ;
import 'package:flutter/material.dart' ;
import 'Menus/menu_root.dart';
import 'AppInterface/responsive_layout.dart' ;
import '../AnimationInterface/update_background.dart' ;
import '../Common/pointer_manager.dart' ;

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late final Timer timer ;

  @override
  void initState() {
    super.initState();

    timer = Timer(const Duration(milliseconds: 20), () {
      List<String> listImage = [
        'animated-bird.gif',         'broken_tile_part23.png',  'piece.png',
        'back_circle_example.png',   'broken_tile_part3.png',   'puzzle_win.png',
        'background_texture.png',    'cracked_tile.png',        'red_circle_example.png',
        'black_circle_example.png',  'ghost.gif',               'simple_rocket.gif',
        'board.png',                 'heart.gif',               'tile_example.png',
        'broken_tile_part1.png',     'Ink_covered.png',         'water_droplet.png',
        'broken_tile_part12.png',    'ink_droplet.png',         'yellow_circle_example.png',
        'broken_tile_part13.png',    'Ink_splash.png',
        'broken_tile_part2.png',     'nasty_rocket.gif',
      ] ;

      for (int m = 0 ; m < listImage.length ; m++) {
        precacheImage(
            Image.asset('lib/images/' + listImage[m]).image,
            context
        ) ;
      }
    }) ;
  }

  @override
  void dispose() {
    timer.cancel() ;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: NotificationListener<SizeChangedLayoutNotification>(
          onNotification: (_){
            PointerManager.updatePlayableSize?.call() ;
            PointerManager.updateBoardSize?.call() ;
            return true ;
          },
          child: SizeChangedLayoutNotifier(
            child: Scaffold(
              backgroundColor: Colors.black,
              body: Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('lib/images/background_texture.png'),
                    repeat: ImageRepeat.repeat,
                  ),
                ),
                child: Stack(
                  children: [
                    AnimatedBuilder(
                        animation: UpdateBackground(),
                        builder: (_, child) {
                          return AnimatedContainer(
                            duration: const Duration(milliseconds: 1000),
                            color: UpdateBackground.isDanger
                                ? const Color(0x60FF4000)
                                : const Color(0x6030A0FF),
                          ) ;
                        }
                    ),
                    const ResponsiveLayout(),
                    const MenuRoot(),
                  ],
                ),
              ),
            ),
          ),
        ),
    );
  }
}
