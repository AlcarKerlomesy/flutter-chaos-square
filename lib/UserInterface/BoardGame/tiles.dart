import 'dart:async';
import 'dart:ui' as ui;
import 'package:vector_math/vector_math_64.dart' show Vector2 ;
import 'package:flutter/material.dart' ;
import 'tile_number.dart' ;
import '../../Common/pointer_manager.dart' ;
import '../../AnimationInterface/update_tile.dart' ;
import '../../Common/coordinate_transform.dart';

double toWidgetCoordinate(double x) {return 2.0*x/3.0 - 1 ;}

class PuzzleTile extends StatefulWidget {
  final TileState tileState ;
  final bool isGhost ;
  final bool isBrokenPiece ;
  const PuzzleTile({Key? key,
    required this.tileState,
    required this.isGhost,
    required this.isBrokenPiece
  }) : super(key: key);

  @override
  _PuzzleTileState createState() => _PuzzleTileState();
}

class _PuzzleTileState extends State<PuzzleTile> {
  void pointerDownCallback(PointerEvent details) {
    if (!widget.isBrokenPiece) {
      PointerManager.pointerDown(details) ;
    }
  }

  void pointerMoveCallback(PointerEvent details) {
    if (!widget.isBrokenPiece) {
      PointerManager.pointerMove(details) ;
    }
  }

  void pointerUpCallback(PointerEvent details) {
    if (!widget.isBrokenPiece) {
      PointerManager.pointerUp() ;
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget stateWidget = FractionallySizedBox(
      heightFactor: 0.252,
      widthFactor: 0.252,
      child: AnimatedBuilder(
          animation: UpdateTileState(widget.tileState),
          builder: (_, child) {
            bool doApplyShader = widget.tileState.explodedState > 1 &&
                widget.tileState.explodedState < 30 ;
            Widget tileStateStack = Stack(
              children: tileRenderStack(widget.tileState, widget.isGhost),
            ) ;

            return doApplyShader ? FutureBuilder<ui.Image>(
              future: loadAssetImage(imagePath(widget.tileState.explodedState)),
              builder: (context, img) {
                Matrix4 transformMatrix = Matrix4.identity() ;

                if (PointerManager.boardIsSet && img.hasData) {
                  double matrixScale = PointerManager.boardSize[0]/4/img.data!.width ;
                  transformMatrix.scale(matrixScale, matrixScale, matrixScale) ;
                }

                return img.hasData
                    ? ShaderMask(
                  blendMode: BlendMode.modulate,
                  shaderCallback: (bounds) =>
                      ImageShader(
                        img.data!,
                        TileMode.clamp,
                        TileMode.clamp,
                        transformMatrix.storage,
                      ),
                  child: tileStateStack,
                ) : tileStateStack ;
              },
            ) : tileStateStack ;
          }
      )
    ) ;

    Widget alignWidget = AnimatedBuilder(
      animation: UpdateTilePosition(widget.tileState),
      child: widget.isGhost || widget.isBrokenPiece
        ? stateWidget
        : Listener(
            behavior: HitTestBehavior.opaque,
            onPointerDown: pointerDownCallback,
            onPointerMove: pointerMoveCallback,
            onPointerUp: pointerUpCallback,
            child: stateWidget
        ),
      builder: (_, child) {
        Vector2 widgetPosition = puzzleToUIPosition(
            widget.tileState.position, 0.25
        ) ;

        return AnimatedAlign(
          alignment: Alignment(widgetPosition.x, widgetPosition.y),
          duration: const Duration(milliseconds: 200),
          curve: Curves.easeOutCubic,
          child: child,
        ) ;
      }
    );

    return AnimatedBuilder(
      animation: UpdateTileVisibility(widget.tileState),
      child: widget.isGhost
        ? Opacity(
          opacity: 0.5,
          child: alignWidget,
        ) : alignWidget,
      builder: (_, child) {
        bool doDraw = (!widget.isGhost || UpdateTileVisibility.isSelected) &&
            widget.tileState.explodedState != 1 ;

        return Visibility(
            visible: doDraw,
            child: child!,
        ) ;
      }
    ) ;
  }
}

String imagePath(int explodedState) {
  switch (explodedState) {
    case 2: return 'lib/images/broken_tile_part1.png' ;
    case 3: return 'lib/images/broken_tile_part2.png' ;
    case 5: return 'lib/images/broken_tile_part3.png' ;
    case 6: return 'lib/images/broken_tile_part12.png' ;
    case 10: return 'lib/images/broken_tile_part13.png' ;
    case 15: return 'lib/images/broken_tile_part23.png' ;
    default: return '' ;
  }
}

List<Widget> tileRenderStack(TileState tileState, bool isGhost) {
  List<Widget> mainStack = [] ;
  Color curColor = Colors.black ;

  if (tileState.dirtyness == 2) {
    mainStack += [Container(
      decoration: BoxDecoration(
        border: Border.all(width: 1, color: const Color(0xA0000000)),
        image: const DecorationImage(
          image: AssetImage('lib/images/Ink_covered.png'),
          fit: BoxFit.cover,
        ),
      ),
    )] ;
  } else {
    mainStack += [Container(
      decoration: BoxDecoration(
        border: Border.all(width: 1, color: const Color(0xA0000000)),
        image: const DecorationImage(
          image: AssetImage('lib/images/piece.png'),
          fit: BoxFit.cover,
        ),
      ),
    )] ;

    if (tileState.circleState != 0) {
      switch (tileState.circleState) {
        case 1: curColor = Colors.black ; break ;
        case 2: curColor = Colors.red ; break ;
        case 3: curColor = Colors.yellow ; break ;
      }

      mainStack += [Opacity(
        opacity: 0.6,
        child: Center(
          child: FractionallySizedBox(
              heightFactor: 0.8,
              widthFactor: 0.8,
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: const Color(0xA0000000)),
                  shape: BoxShape.circle,
                  color: curColor,
                ),
              )
          ),
        ),
      )] ;
    }

    mainStack += [TileNumber(centerText: tileState.number.toString())] ;

    if (tileState.dirtyness == 1) {
      mainStack += [Image.asset('lib/images/Ink_splash.png',
        fit: BoxFit.cover,
      )] ;
    }
  }

  if (tileState.explodedState == 30) {
    mainStack += [Image.asset('lib/images/cracked_tile.png',
      fit: BoxFit.cover,
    )] ;
  }

  if (isGhost) {
    mainStack += [Container(color: const Color(0x80FFA500),)] ;
  }

  return mainStack ;
}

Future<ui.Image> loadAssetImage(String path) async {
  Completer<ui.Image> completer = Completer() ;
  AssetImage img = AssetImage(path) ;
  
  img.resolve(const ImageConfiguration())
    .addListener(ImageStreamListener((ImageInfo info, bool _) {
      completer.complete(info.image) ;
  })) ;
  
  return await completer.future ;
}