import 'package:flutter/material.dart' ;
import 'tiles.dart' ;
import 'circle_patch.dart' ;
import 'foes_draw.dart';
import '../../GameMechanics/PuzzleMechanics/puzzle.dart';
import '../../Common/pointer_manager.dart' ;
import '../../AnimationInterface/update_puzzle_visibility.dart';
import '../../Common/constants.dart' as cst ;

class BoardGame extends StatefulWidget {
  const BoardGame({Key? key}) : super(key: key);

  @override
  _BoardGameState createState() => _BoardGameState();
}

class _BoardGameState extends State<BoardGame> {

  void pointerDownCallback(PointerEvent details) {
    PointerManager.pointerDownOutOfBound(details) ;
  }

  void pointerMoveCallback(PointerEvent details) {
    PointerManager.pointerMoveOutOfBound(details) ;
  }

  void pointerUpCallback(PointerEvent details) {
    PointerManager.pointerUpOutOfBound() ;
  }

  updateBoardSize() {
    PointerManager.setLayoutParameters(
      context.findRenderObject()! as RenderBox, true
    ) ;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      updateBoardSize() ;
    }) ;

    PointerManager.updateBoardSize = updateBoardSize ;
  }

  @override
  void dispose() {
    PointerManager.updateBoardSize = null ;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: UpdatePuzzleVisibility(),
      child: Listener(
        behavior: HitTestBehavior.opaque,
        onPointerDown: pointerDownCallback,
        onPointerMove: pointerMoveCallback,
        onPointerUp: pointerUpCallback,
        child: Stack(
          children: <Widget>[const CircleStack()]
              + List.generate(15, (index) => PuzzleTile(
                tileState: Puzzle.tilesState[index+1],
                isGhost: false,
                isBrokenPiece: false,)
              )
              + [const PieceStack()]
              + [PuzzleTile(
                tileState: Puzzle.tilesState[0],
                isGhost: true,
                isBrokenPiece: false,
              )]
              + [const FoesStack()],
        ),
      ),
      builder: (_, child) {
        return Offstage(
            offstage: UpdatePuzzleVisibility.hidePuzzle,
            child: child!
        ) ;
      }
    ) ;
  }
}

class PieceStack extends StatelessWidget {
  const PieceStack({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: List.generate(cst.listPieceLength,
              (index) => PuzzleTile(
            tileState: Puzzle.tilePiecesState[index],
            isGhost: false,
            isBrokenPiece: true,
          )
      ),
    ) ;
  }
}
