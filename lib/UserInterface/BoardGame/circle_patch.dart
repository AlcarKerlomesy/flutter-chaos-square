import 'package:flutter/material.dart' ;
import 'package:vector_math/vector_math_64.dart' show Vector2 ;
import '../../AnimationInterface/update_circle_patch.dart' ;
import '../../Common/coordinate_transform.dart';
import '../../Common/constants.dart' as cst ;
import 'tile_number.dart' ;

class CircleStack extends StatelessWidget {
  const CircleStack({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: UpdateCirclePatch(),
        builder: (_, child) {
          return Visibility(
            visible: UpdateCirclePatch.circleListLength > 0,
            child: Stack(
              children: List.generate(UpdateCirclePatch.circleListLength,
                (index) => CirclePatch(
                  circleState: UpdateCirclePatch.getCircleState(index),
                )
              )
            )
          ) ;
        }
    );
  }
}

class CirclePatch extends StatelessWidget {
  final CircleState circleState ;
  const CirclePatch({Key? key,
    required this.circleState
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Vector2 position = adaptUiPosition(circleState.position, cst.circleSize) ;

    return Align(
      alignment: Alignment(position.x, position.y),
      child: FractionallySizedBox(
          heightFactor: cst.circleSize,
          widthFactor: cst.circleSize,
          child: Opacity(
              opacity: cst.circleOpacity,
              child: Container(
                decoration: const BoxDecoration(
                  color: Colors.yellow,
                  shape: BoxShape.circle,
                ),
                child: TileNumber(centerText: circleState.number.toString(),),
              ),
          )
      ),
    );
  }
}