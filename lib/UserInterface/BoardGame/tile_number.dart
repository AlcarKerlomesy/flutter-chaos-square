import 'package:flutter/material.dart' ;
import '../../Common/constants.dart' as cst;

class TileNumber extends StatelessWidget {
  final String centerText ;
  const TileNumber({Key? key, required this.centerText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return Stack(
            children: [
              Text(
                centerText,
                style: TextStyle(
                  fontSize: constraints.maxWidth*cst.numberFontSize,
                  foreground: Paint()
                    ..style = PaintingStyle.stroke
                    ..strokeWidth = constraints.maxWidth*cst.numberStrokeWidth
                    ..color = Colors.black,
                ),
              ),
              Text(
                  centerText,
                  style: TextStyle(
                    fontSize: constraints.maxWidth*cst.numberFontSize,
                    color: const Color(0xFF5050FF),
                  )
              )
            ],
          ) ;
        },
      ),
    ) ;
  }
}