import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' ;
import 'package:vector_math/vector_math_64.dart' show Vector2 ;
import '../../AnimationInterface/update_foes.dart' ;

class FoesStack extends StatelessWidget {
  const FoesStack({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
        child: AnimatedBuilder(
            animation: UpdateFoes(),
            builder: (_, child) {
              List<Widget> listFoes = List.generate(UpdateFoes.listFoesLength,
                      (index) => SingleFoe(foeData: UpdateFoes.getFoe(index))) ;

              return Stack(
                  fit: StackFit.expand,
                  children: listFoes
              ) ;
            }
        )
    ) ;
  }
}

class SingleFoe extends StatelessWidget {
  final FoeActor foeData ;
  const SingleFoe({Key? key, required this.foeData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Vector2 position = foeData.getTransformPosition() ;
    return Opacity(
      opacity: foeData.opacity,
      child: Transform(
        alignment: Alignment(position.x, position.y),
        transform: foeData.getTransformMatrix(),
        child: Image.asset(getFoeImage(foeData.actorGraphic),
          fit: BoxFit.contain,
        ),
      ),
    ) ;
  }
}

String getFoeImage(ActorGraphic foeGraphic) {
  switch (foeGraphic) {
    case ActorGraphic.bird:
      return 'lib/images/animated-bird.gif' ;
    case ActorGraphic.ghost:
      return 'lib/images/ghost.gif' ;
    case ActorGraphic.rocket:
      return 'lib/images/simple_rocket.gif' ;
    case ActorGraphic.nastyRocket:
      return 'lib/images/nasty_rocket.gif' ;
    case ActorGraphic.inkDroplet:
      return 'lib/images/ink_droplet.png' ;
    case ActorGraphic.waterDroplet:
      return 'lib/images/water_droplet.png' ;
    case ActorGraphic.heart:
      return 'lib/images/heart.gif' ;
  }
}