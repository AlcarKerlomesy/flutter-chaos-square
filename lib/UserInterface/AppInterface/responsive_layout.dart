import 'package:flutter/material.dart' ;
import '../../Common/constants.dart' as cst;
import 'playable_area.dart' ;
import 'bars_content.dart' ;

class ResponsiveLayout extends StatelessWidget {
  const ResponsiveLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          if (constraints.maxHeight >= cst.maxPlayableSize + cst.topBarHeight ||
              constraints.maxHeight - cst.topBarHeight >= constraints.maxWidth - cst.sideBarWidth) {
            return Center(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxWidth: constraints.maxWidth > cst.maxPlayableSize + cst.sideBarWidth ?
                  cst.maxPlayableSize : constraints.maxWidth,
                ),
                child: const AppContent(showTopBar: true),
              ),
            );
          } else {
            return const AppContent(showTopBar: false) ;
          }
        });
  }
}

class AppContent extends StatelessWidget {
  final bool showTopBar ;
  const AppContent({Key? key, required this.showTopBar}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BoxDecoration topSideBarDeco = BoxDecoration(
      color: const Color(0xFFCE836C),
      border: Border.all(
        color: Colors.brown,
        width: 2,
      ),
    ) ;

    return Column(
      children: [
        showTopBar ? Container(
          height: cst.topBarHeight,
          decoration: topSideBarDeco,
          child: const TopSideBar(isTop: true,),
        ) : const SizedBox.shrink(),
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              showTopBar ? const SizedBox.shrink() : FractionallySizedBox(
                  heightFactor: cst.sideBarHeightRatio,
                  child: Container(
                    width: cst.sideBarWidth,
                    decoration: topSideBarDeco,
                    child: const TopSideBar(isTop: false,),
                  )
              ),
              const Flexible(
                child: PlayableArea(),
              ),
            ],
          ),
        ),
      ],
    );
  }
}