import 'package:flutter/material.dart' ;
import '../../Common/constants.dart' as cst;
import '../BoardGame/board_game.dart' ;
import '../../Common/pointer_manager.dart' ;
import '../../AnimationInterface/update_red_pointer.dart' ;

class PlayableArea extends StatelessWidget {
  const PlayableArea({Key? key}) : super(key: key);

  void pointerDown(PointerEvent details) {
    PointerManager.pointerDownOutOfBound(details) ;
    PointerManager.blockPointerCallback = false ;
  }

  void pointerMove(PointerEvent details) {
    PointerManager.pointerMoveOutOfBound(details) ;
    PointerManager.blockPointerCallback = false ;
  }

  void pointerUp(PointerEvent details) {
    PointerManager.pointerUpOutOfBound() ;
    PointerManager.blockPointerCallback = false ;
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: Listener(
        behavior: HitTestBehavior.opaque,
        onPointerDown: pointerDown,
        onPointerMove: pointerMove,
        onPointerUp: pointerUp,
        child: SizedBox(
          width: cst.maxPlayableSize,
          height: cst.maxPlayableSize,
          child: Stack(
            children: [
              Positioned.fill(
                child: FractionallySizedBox(
                  widthFactor: cst.boardSizeRatio,
                  heightFactor: cst.boardSizeRatio,
                  alignment: Alignment.center,
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.black),
                      image: const DecorationImage(
                        image: AssetImage('lib/images/board.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: const FractionallySizedBox(
                      widthFactor: 0.89,
                      heightFactor: 0.885,
                      alignment: Alignment(0.05, 0.02),
                      child: BoardGame(),
                    ),
                  ),
                ),
              ),
              const RedPointer(),
            ]
          ),
        ),
      ),
    );
  }
}

class RedPointer extends StatefulWidget {
  const RedPointer({Key? key}) : super(key: key);

  @override
  _RedPointerState createState() => _RedPointerState();
}

class _RedPointerState extends State<RedPointer> {

  updatePlayableAreaSize() {
    PointerManager.setLayoutParameters(
      context.findRenderObject()! as RenderBox, false) ;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      updatePlayableAreaSize() ;
    }) ;

    PointerManager.updatePlayableSize = updatePlayableAreaSize ;
  }

  @override
  void dispose() {
    PointerManager.updatePlayableSize = null ;

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: UpdateRedPointer(),
      child: FractionallySizedBox(
        heightFactor: cst.sizeRedPointer,
        widthFactor: cst.sizeRedPointer,
        child: Container(
          decoration: const BoxDecoration(
              gradient: RadialGradient(
                  colors: [
                    Color(0xB0FF0000), Color(0x00000000),
                  ]
              )
          ),
        ),
      ),
      builder: (_, child) {
        if (UpdateRedPointer.isOutOfBound) {
          return Align(
              alignment: Alignment(
                  UpdateRedPointer.pointerPosition.x,
                  UpdateRedPointer.pointerPosition.y),
              child: child
          ) ;
        } else {
          return const SizedBox.shrink() ;
        }
      }
    ) ;
  }
}
