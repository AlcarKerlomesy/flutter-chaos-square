import 'package:flutter/material.dart' ;
import '../../GameMechanics/game_master.dart';
import '../../AnimationInterface/update_counters.dart' ;
import '../../Common/constants.dart' as cst ;

class TopSideBar extends StatelessWidget {
  final bool isTop ;
  const TopSideBar({Key? key, required this.isTop}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Align(
          alignment: isTop
              ? const Alignment(-cst.barElementAlign, 0)
              : const Alignment(0, -cst.barElementAlign),
          child: AnimatedBuilder(
            animation: UpdateNumMoves(),
            builder: (_, child) {
              return Text(
                'MOVES\n' + UpdateNumMoves.numMoves.toString(),
                textAlign: TextAlign.center,
                overflow: TextOverflow.fade,
                style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w900
                ),
              ) ;
            },
          )
        ),
        Align(
            alignment: isTop
                ? const Alignment(0, 0)
                : const Alignment(0, 0),
            child: AnimatedBuilder(
              animation: UpdateTimer(),
              builder: (_, child) {
                return Visibility(
                  visible: UpdateTimer.isTimerVisible,
                  child: Text(
                    'TIME\n'
                        + UpdateTimer.timerDisplay,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.fade,
                    style: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w900
                    ),
                  ),
                ) ;
              },
            )
        ),
        Align(
          alignment: isTop
              ? const Alignment(cst.barElementAlign, 0)
              : const Alignment(0, cst.barElementAlign),
          child: Tooltip(
            message: 'Pause',
            child: ElevatedButton(
              onPressed: () {GameMaster.pauseGame();},
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.brown),
                shape: MaterialStateProperty.all<OutlinedBorder>(const CircleBorder()),
              ),
              child: const Icon(Icons.pause,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
