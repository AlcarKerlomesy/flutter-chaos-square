import 'package:flutter/material.dart' ;
import 'dart:ui' as ui;
import '../../Common/constants.dart' as cst;
import '../../GameMechanics/game_master.dart' ;
import '../../AnimationInterface/update_counters.dart' ;

class WinMenu extends StatelessWidget {
  const WinMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ButtonStyle defaultStyle = ButtonStyle(
      fixedSize: MaterialStateProperty.all(const Size.fromWidth(cst.widthButton)),
    ) ;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('You Won in ' + UpdateNumMoves.numMoves.toString() + ' Moves !!!',
          style: TextStyle(
            fontSize: 40,
            fontWeight: FontWeight.bold,
            foreground: Paint()
              ..shader = ui.Gradient.linear(
                const Offset(0, 0),
                const Offset(100, 50),
                <Color>[
                  Colors.red,
                  Colors.yellow,
                ],
                null,
                TileMode.mirror,
              ),
          ),
        ),
        const SizedBox(height: 50),
        ElevatedButton(
            onPressed: () {GameMaster.abortGame();},
            style: defaultStyle,
            child: const Text('To Menu')),
      ],
    ) ;
  }
}
