import 'package:flutter/material.dart' ;
import '../../Common/constants.dart' as cst;
import '../../GameMechanics/game_master.dart' ;

class PauseMenu extends StatelessWidget {
  const PauseMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ButtonStyle defaultStyle = ButtonStyle(
      fixedSize: MaterialStateProperty.all(const Size.fromWidth(cst.widthButton)),
    ) ;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text("Game Paused",
            style: TextStyle(
              color: Colors.lightGreenAccent,
              fontSize: 40, fontWeight: FontWeight.bold,
            )
        ),
        const SizedBox(height: 50),
        ElevatedButton(
            onPressed: () {GameMaster.continueGame();},
            style: defaultStyle,
            child: const Text('Continue')),
        const SizedBox(height: 20),
        ElevatedButton(
            onPressed: () {GameMaster.abortGame();},
            style: defaultStyle,
            child: const Text('To Menu')),
      ],
    ) ;
  }
}