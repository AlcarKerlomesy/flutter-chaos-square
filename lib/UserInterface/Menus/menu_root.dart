import 'package:flutter/material.dart' ;
import '../../AnimationInterface/update_menu.dart' ;

import 'pause_menu.dart' ;
import 'start_instructions_menu.dart' ;
import 'win_menu.dart' ;
import 'lose_menu.dart' ;

class MenuRoot extends StatelessWidget {
  const MenuRoot({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: UpdateMenu(),
      builder: (_, child) {
        Widget shownMenu ;
        bool darkScreen = true ;

        switch (UpdateMenu.menuSelect) {
          case MenuSelect.none:
            shownMenu = const SizedBox.shrink() ;
            darkScreen = false ; break ;
          case MenuSelect.startMenu:
            shownMenu = const StartInstructionsMenu() ; break ;
          case MenuSelect.pauseMenu:
            shownMenu = const PauseMenu() ; break ;
          case MenuSelect.winMenu:
            shownMenu = const WinMenu() ; break ;
          case MenuSelect.loseMenu:
            shownMenu = const LoseMenu() ; break ;
        }

        return darkScreen
          ? Container(
            decoration: const BoxDecoration(color: Color(0xA0000000)),
            child: Center(
              child: shownMenu,
            ),
          )
          : shownMenu ;
      }
    ) ;
  }
}
