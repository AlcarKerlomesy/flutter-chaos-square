import 'package:flutter/material.dart' ;
import '../../Common/constants.dart' as cst;
import '../../GameMechanics/game_master.dart' ;
import '../../AnimationInterface/update_counters.dart' ;

class LoseMenu extends StatelessWidget {
  const LoseMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ButtonStyle defaultStyle = ButtonStyle(
      fixedSize: MaterialStateProperty.all(const Size.fromWidth(cst.widthButton)),
    ) ;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text("You Lost !",
            style: TextStyle(
              color: Colors.red, fontSize: 40,
              fontWeight: FontWeight.bold
            ),
            textAlign: TextAlign.center,
        ),
        const SizedBox(height: 20),
        Text(UpdateTimer.timerDisplay + " Remaining",
          style: const TextStyle(color: Colors.orange, fontSize: 40),
          textAlign: TextAlign.center,
        ),
        const SizedBox(height: 50),
        ElevatedButton(
            onPressed: () {GameMaster.abortGame();},
            style: defaultStyle,
            child: const Text('To Menu')),
      ],
    ) ;
  }
}