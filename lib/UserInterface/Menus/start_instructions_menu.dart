import 'package:flutter/material.dart' ;

import 'menu_instructions/acknowledgment.dart';
import 'menu_instructions/start_new_game.dart';
import 'menu_instructions/instructions_classic.dart';
import 'menu_instructions/instructions_chaos.dart';

class StartInstructionsMenu extends StatelessWidget {
  const StartInstructionsMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PageController controller = PageController(initialPage: 1);

    return PageView(
      controller: controller,
      children: [
        Acknowledgement(
          scrollPageLeft: (){
            controller.animateToPage(3, duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
          },
          scrollPageRight: (){
            controller.animateToPage(1, duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
          },
        ),
        StartMenu(
          scrollPageLeft: (){
            controller.animateToPage(0, duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
          },
          scrollPageRight: (){
            controller.animateToPage(2, duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
          },
        ),
        InstructionsClassic(
          scrollPageLeft: (){
            controller.animateToPage(1, duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
          },
          scrollPageRight: (){
            controller.animateToPage(3, duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
          },
        ),
        InstructionsChaos(
          scrollPageLeft: (){
            controller.animateToPage(2, duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
          },
          scrollPageRight: (){
            controller.animateToPage(0, duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
          },
        ),
      ]
    );
  }
}
