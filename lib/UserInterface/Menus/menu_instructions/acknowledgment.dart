import 'package:flutter/material.dart' ;
import '../../../Common/callback_typedef.dart';

import 'menu_instructions_layout.dart';

class Acknowledgement extends StatelessWidget {
  final Callback scrollPageLeft;
  final Callback scrollPageRight;
  const Acknowledgement({Key? key,
    required this.scrollPageLeft,
    required this.scrollPageRight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InstructionsLayout(
      textLeft: SideText(
        isLeft: true,
        text: "Back to Instructions (Chaos)",
        onPressed: scrollPageLeft,
      ),
      textRight: SideText(
        isLeft: false,
        text: "Start Menu",
        onPressed: scrollPageRight,
      ),
      child: Column(
        children: const [
          SizedBox(height: 50),
          Text("Acknowledgements",
              style: styleTitle,
          ),
          SizedBox(height: 70),
          Text(
            "The geeky side:",
            style: TextStyle(
              color: Colors.lightBlueAccent, fontSize: 30,
              decoration: TextDecoration.underline,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 50),
          Text(
            "The Flutter community",
            style: styleParagraph,
            textAlign: TextAlign.center,
          ),
          paragraphSeparation,
          Text(
            "The Dart community",
            style: styleParagraph,
            textAlign: TextAlign.center,
          ),
          paragraphSeparation,
          Text(
            "Android Studio",
            style: styleParagraph,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 70),
          Text(
            "The artistic side:",
            style: TextStyle(
              color: Colors.lightBlueAccent, fontSize: 30,
              decoration: TextDecoration.underline,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 50),
          SelectableText(
            "https://www.animatedimages.org/",
            style: styleParagraph,
            textAlign: TextAlign.center,
          ),
          paragraphSeparation,
          SelectableText(
            "https://cpetry.github.io/TextureGenerator-Online/",
            style: styleParagraph,
            textAlign: TextAlign.center,
          ),
          paragraphSeparation,
          Text(
            "Krita",
            style: styleParagraph,
            textAlign: TextAlign.center,
          ),
          paragraphSeparation,
          Text(
            "LibreOffice",
            style: styleParagraph,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 70),
          Text(
            "Also a big thanks to those who play and share this game.",
            style: TextStyle(color: Colors.lightBlueAccent, fontSize: 30),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 70),
          Text(
            "This game was made by Alcar Kerlomesy",
            style: TextStyle(color: Colors.white, fontSize: 20),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 50),
        ],
      ),
    ) ;
  }
}