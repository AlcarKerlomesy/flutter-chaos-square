import 'package:flutter/material.dart' ;
import '../../../Common/callback_typedef.dart';

import 'menu_instructions_layout.dart';

class InstructionsChaos extends StatelessWidget {
  final Callback scrollPageLeft;
  final Callback scrollPageRight;
  const InstructionsChaos({Key? key,
    required this.scrollPageLeft,
    required this.scrollPageRight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InstructionsLayout(
      textLeft: SideText(
        isLeft: true,
        text: "Instructions",
        onPressed: scrollPageLeft,
      ),
      textRight: SideText(
        isLeft: false,
        text: "Back to Acknowledgements",
        onPressed: scrollPageRight,
      ),
      child: Column(
        children: [
          const SizedBox(height: 50),
          const Text("Instructions (Chaos)",
              style: styleTitle
          ),
          const SizedBox(height: 30),
          SizedBox(
            height: 70,
            child: Row(
              children: [
                const Spacer(flex: 5),
                Image.asset('lib/images/yellow_circle_example.png',
                  fit: BoxFit.contain,
                ),
                const Spacer(flex: 2),
                Image.asset('lib/images/red_circle_example.png',
                  fit: BoxFit.contain,
                ),
                const Spacer(flex: 2),
                Image.asset('lib/images/black_circle_example.png',
                  fit: BoxFit.contain,
                ),
                const Spacer(flex: 5),
              ],
            ),
          ),
          paragraphSeparation,
          const Text(
            "The chaos mode has three phases. To enter the second phase, the 4 markings drawn on the board must be below the corresponding tiles, which causes circles to appear on those tiles. You can skip the first phase for a cost of 100 moves.",
            style: styleParagraph,
            textAlign: TextAlign.justify,
          ),
          paragraphSeparation,
          const Text(
            "Once the circles appear, a wave of cute creatures followed by many more nasty ones are unleashed. All four circles must survive. Once the situation has calmed down, you can end the game like a normal game of Fifteen. Beware, depending on how you got through the assault, you may not be able to complete the puzzle. Here are the enemies you will encounter:",
            style: styleParagraph,
            textAlign: TextAlign.justify,
          ),
          paragraphSeparation,
          const Align(
            alignment: Alignment(-1, 0),
            child: Text(
              "Here are the enemies you will encounter:",
              style: styleParagraph,
              textAlign: TextAlign.justify,
            ),
          ),
          paragraphSeparation,
          SizedBox(
            height: 70,
            child: Row(
              children: [
                const Spacer(flex: 5),
                Image.asset('lib/images/ink_droplet.png',
                  fit: BoxFit.contain,
                ),
                const Spacer(flex: 2),
                Image.asset('lib/images/water_droplet.png',
                  fit: BoxFit.contain,
                ),
                const Spacer(flex: 5),
              ],
            ),
          ),
          paragraphSeparation,
          const Text(
            "The ink drop is not a direct threat, but it covers the first tile it touches with a thick layer of ink. This ink can be removed with water.",
            style: styleParagraph,
            textAlign: TextAlign.justify,
          ),
          paragraphSeparation,
          SizedBox(
            height: 70,
            child: Row(
              children: [
                const Spacer(flex: 5),
                Image.asset('lib/images/ghost.gif',
                  fit: BoxFit.contain,
                ),
                const Spacer(flex: 2),
                Image.asset('lib/images/heart.gif',
                  fit: BoxFit.contain,
                ),
                const Spacer(flex: 5),
              ],
            ),
          ),
          paragraphSeparation,
          const Text(
            "The ghosts ignore tiles, but not circles. When a yellow circle is hit, it turns red. If it is hit a second time, the circle dies by turning black, and you lose the game. A flying heart, which also passes through the tiles, can heal the circles.",
            style: styleParagraph,
            textAlign: TextAlign.justify,
          ),
          paragraphSeparation,
          Row(
            children: [
              const Spacer(flex: 5),
              SizedBox(
                width: 70, height: 70,
                child: Image.asset('lib/images/simple_rocket.gif',
                  fit: BoxFit.contain,
                ),
              ),
              const Spacer(flex: 2),
              SizedBox(
                width: 70, height: 70,
                child: Image.asset('lib/images/nasty_rocket.gif',
                  fit: BoxFit.contain,
                ),
              ),
              const Spacer(flex: 5),
            ],
          ),
          paragraphSeparation,
          const Text(
            "The last enemy are the missiles, which crack or blow up the tiles they hit. There are two flavors of missiles: the pretty ones and the ugly ones. The pretty missiles go straight, cracking an unscathed tile and blowing up a cracked tile. The ugly missiles go to a randomly chosen circle, and blow up the first tile they hit. The pieces of an exploded tile can be put back together on the board. Be careful, a piece can only stick to another one if it belonged to the same tile. Also, an uncompleted tile takes the same place as a normal tile, but does not block missiles. Tip: The ugly missiles are the first and last foes you will encounter.",
            style: styleParagraph,
            textAlign: TextAlign.justify,
          ),
          const SizedBox(height: 50),
        ],
      ),
    ) ;
  }
}