import 'package:flutter/material.dart' ;
import '../../../Common/callback_typedef.dart';

const TextStyle styleTitle = TextStyle(
  color: Colors.orange, fontSize: 40
) ;
const TextStyle styleParagraph = TextStyle(
  color: Colors.lightGreenAccent, fontSize: 20
) ;
const TextStyle styleParagraphBold = TextStyle(
  color: Colors.lightBlueAccent, fontSize: 22, fontWeight: FontWeight.bold
) ;
const Widget paragraphSeparation = SizedBox(height: 20) ;

class InstructionsLayout extends StatelessWidget {
  final Widget child;
  final Widget? textLeft, textRight ;
  const InstructionsLayout({Key? key,
    required this.child,
    this.textLeft,
    this.textRight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: Color(0x40000000)),
      child: Row(
        children: [
          const SizedBox(width: 10),
          Expanded(
              child: textLeft ?? const SizedBox.shrink()
          ),
          Expanded(
            flex: 15,
            child: Center(
                child: ConstrainedBox(
                  constraints: const BoxConstraints(maxWidth: 800),
                  child: SingleChildScrollView(
                    child: child,
                  ),
                )
            ),
          ),
          Expanded(
              child: textRight ?? const SizedBox.shrink()
          ),
          const SizedBox(width: 10)
        ],
      ),
    ) ;
  }
}


class SideText extends StatelessWidget {
  final bool isLeft;
  final String text;
  final Callback onPressed;
  const SideText({Key? key,
    required this.isLeft,
    required this.text,
    required this.onPressed
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment(isLeft ? -1 : 1, 0),
      child: RotatedBox(
        quarterTurns: isLeft ? 1 : 3,
        child: TextButton(
            onPressed: onPressed,
            child: Text(text,
                style: const TextStyle(
                  color: Color(0x40FFFFFF),
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                )
            )
        ),
      ),
    ) ;
  }
}