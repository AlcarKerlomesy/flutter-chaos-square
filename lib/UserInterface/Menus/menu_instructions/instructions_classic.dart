import 'package:flutter/material.dart' ;
import '../../../Common/callback_typedef.dart';

import 'menu_instructions_layout.dart';

class InstructionsClassic extends StatelessWidget {
  final Callback scrollPageLeft;
  final Callback scrollPageRight;
  const InstructionsClassic({Key? key,
    required this.scrollPageLeft,
    required this.scrollPageRight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InstructionsLayout(
      textLeft: SideText(
        isLeft: true,
        text: "Start Menu",
        onPressed: scrollPageLeft,
      ),
      textRight: SideText(
        isLeft: false,
        text: "Instructions (Chaos)",
        onPressed: scrollPageRight,
      ),
      child: Column(
        children: [
          const SizedBox(height: 50),
          const Text("Instructions",
            style: styleTitle
          ),
          const SizedBox(height: 30),
          const Align(
            alignment: Alignment(-1, 0),
            child: Text(
              "This game has 3 modes:",
              style: styleParagraph,
            ),
          ),
          paragraphSeparation,
          Row(
            children: [
              Expanded(
                flex: 70,
                child: Column(
                  children: [
                    Row(
                      children: const [
                        SizedBox(
                          width: 100,
                          child: Text("Classic", style: styleParagraphBold),
                        ),
                        Expanded(
                          child: Text(
                            "It is the game of Fifteen, where the objective is to move the tiles to achieve the position shown below. You move a tile by clicking and dragging it. An entire column or row can be moved in this way.",
                            style: styleParagraph,
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ],
                    ),
                    paragraphSeparation,
                    Row(
                      children: const [
                        SizedBox(
                          width: 100,
                          child: Text("Random", style: styleParagraphBold),
                        ),
                        Expanded(
                          child: Text(
                            "A variation of the classic mode where the final position is determined by what is drawn on the board. It is slightly more difficult than the classic mode.",
                            style: styleParagraph,
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ],
                    ),
                    paragraphSeparation,
                    Row(
                      children: const [
                        SizedBox(
                          width: 100,
                          child: Text("Chaos", style: styleParagraphBold),
                        ),
                        Expanded(
                          child: Text(
                            "An arcade game inspired by previous modes. More instructions on the right page.",
                            style: styleParagraph,
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Spacer(flex: 2),
              Expanded(
                flex: 10,
                child: Column(
                  children: [
                    Image.asset('lib/images/tile_example.png',
                      fit: BoxFit.contain,
                    ),
                    const SizedBox(height: 30),
                    Image.asset('lib/images/back_circle_example.png',
                      fit: BoxFit.contain,
                    ),
                  ],
                ),
              ),
              const Spacer(flex: 1),
            ]
          ),
          paragraphSeparation,
          FractionallySizedBox(
            widthFactor: 0.5,
            child: Image.asset('lib/images/puzzle_win.png'),
          ),
          const SizedBox(height: 50),
        ],
      ),
    ) ;
  }
}
