import 'package:flutter/material.dart' ;
import '../../../Common/constants.dart' as cst;
import '../../../Common/callback_typedef.dart';
import '../../../GameMechanics/game_master.dart' ;

import 'menu_instructions_layout.dart';

class StartMenu extends StatelessWidget {
  final Callback scrollPageLeft;
  final Callback scrollPageRight;
  const StartMenu({Key? key,
    required this.scrollPageLeft,
    required this.scrollPageRight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ButtonStyle defaultStyle = ButtonStyle(
      fixedSize: MaterialStateProperty.all(const Size.fromWidth(cst.widthButton)),
    ) ;

    const String titleString = "Chaos Square";
    const List<Color> listColor = [
      Colors.red, Colors.green, Colors.yellow, Colors.orangeAccent,
      Colors.purpleAccent, Colors.white, Colors.orange, Colors.tealAccent,
      Colors.green, Colors.lightBlueAccent, Colors.greenAccent, Colors.yellow
    ];

    List<TextSpan> generateTitle() {
      return List.generate(titleString.length,
          (index) => TextSpan(
            text: titleString[index],
            style: TextStyle(color: listColor[index]),
          )
      ) ;
    }

    return Row(
      children: [
        const SizedBox(width: 10),
        Expanded(
          child: SideText(
            isLeft: true,
            text: 'Acknowledgements',
            onPressed: scrollPageLeft,
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children:[
            RichText(
              text: TextSpan(
                style: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 50),
                children: generateTitle(),
              )
            ),
            const SizedBox(height: 30),
            ElevatedButton(
                onPressed: () {GameMaster.startGame(GameMode.classic);},
                style: defaultStyle,
                child: const Text('Start Classic')
            ),
            const SizedBox(height: 20),
            ElevatedButton(
                onPressed: () {GameMaster.startGame(GameMode.random);},
                style: defaultStyle,
                child: const Text('Start Random')
            ),
            const SizedBox(height: 20),
            ElevatedButton(
                onPressed: () {GameMaster.startGame(GameMode.chaos);},
                style: defaultStyle,
                child: const Text('Start Chaos')
            ),
            const SizedBox(height: 20),
            ElevatedButton(
                onPressed: () {GameMaster.startGame(GameMode.chaosSkipIntro);},
                style: defaultStyle,
                child: const Text('Start Chaos (No Intro)')
            ),
          ]
        ),
        Expanded(
          child: SideText(
            isLeft: false,
            text: 'Instructions',
            onPressed: scrollPageRight,
          )
        ),
        const SizedBox(width: 10),
      ],
    ) ;
  }
}
