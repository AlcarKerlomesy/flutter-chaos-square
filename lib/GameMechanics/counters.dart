import '../Common/callback_typedef.dart' ;

class Counters {
  static int numMoves = 0 ;

  static bool isTimerVisible = false ;
  static Duration curTime = const Duration(seconds: 0) ;
  static int previousSec = 0 ;

  static Callback? redrawMoves ;
  static Callback? redrawTimer ;

  static resetMoveCounter() {
    numMoves = 0 ;
    redrawMoves?.call() ;
  }

  static addMoveCounter(int moves) {
    numMoves += moves ;
    redrawMoves?.call() ;
  }

  static resetTimer() {
    curTime = const Duration(seconds: 0) ;
  }

  static addTime(int milliseconds) {
    curTime += Duration(milliseconds: milliseconds) ;

    if (isTimerVisible && previousSec != curTime.inSeconds) {
      previousSec = curTime.inSeconds ;
      redrawTimer?.call() ;
    }
  }

  static showTimer(bool doShow) {
    if (doShow != isTimerVisible) {
      isTimerVisible = doShow ;
      redrawTimer?.call() ;
    }
  }
}