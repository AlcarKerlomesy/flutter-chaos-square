import 'dart:math' ;
import 'package:vector_math/vector_math_64.dart' ;
import '../Foe/foe_actor.dart' ;
import '../counters.dart' ;

export 'test_scenario.dart' ;
export 'scenario_normal.dart' ;

part 'ClassFunction/event_generators.dart' ;
part 'ClassFunction/foe_description.dart' ;

enum FoeActorType {
  bird,
  ghost,
  missile,
  seekingMissile,
  ink,
  water,
  health
}

typedef ScenarioEvent = bool Function(int) ;

class Scenario {
  List<ScenarioEvent> listFunc = [] ;
  int curPart = 0 ;
  int curTime = 0 ;

  Function(FoeActor) createFoe ;

  Scenario(this.createFoe) {
    Counters.resetTimer() ;
  }

  bool nextAction(int elapsedTime) {
    if (curPart == listFunc.length) {
      return true ;
    }

    while (curPart < listFunc.length && listFunc[curPart](elapsedTime)) {
      curPart++ ;
    }

    return false ;
  }

  wait(int waitDuration) {
    _wait(this, waitDuration) ;
  }

  generateSingleEvent(ScenarioEvent eventCreateFoe) {
    listFunc.add(eventCreateFoe) ;
  }

  generateFoeRandomTimed(int attackDuration, int minWait, int maxWait,
      ScenarioEvent eventCreateFoe) {
    _generateFoeRandomTimed(this, attackDuration, minWait, maxWait,
        eventCreateFoe) ;
  }

  generateFoeRandomNum(int numFoes, int minWait, int maxWait,
      ScenarioEvent eventCreateFoe) {
    _generateFoeRandomNum(this, numFoes, minWait, maxWait,
        eventCreateFoe);
  }

  FoeActor getFoe(FoeActorType foeType) {
    return _getFoe(foeType) ;
  }
}