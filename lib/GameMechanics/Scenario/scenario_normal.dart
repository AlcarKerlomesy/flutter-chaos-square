import 'dart:math' ;
import '../Foe/foe_actor.dart' ;
import 'scenario.dart' ;

class ScenarioNormal extends Scenario {
  ScenarioNormal(Function(FoeActor) createFoe)
      : super(createFoe){
    // Bird intro
    wait(2000) ;
    generateFoeRandomTimed(3000, 100, 1100,
        createSingleFoe(FoeActorType.bird)) ;

    // Seeking missile intro and making some space
    for (int m = 0 ; m < 4 ; m++) {
      wait(500) ;
      generateFoeRandomNum(1, 100, 200,
          createSingleFoe(FoeActorType.seekingMissile, fadeIn: 6000)) ;
      wait(100) ;
      generateFoeRandomTimed(3000, 100, 1100,
          createSingleFoe(FoeActorType.bird)) ;
    }

    //ghost intro
    wait(3500) ;
    generateSingleEvent(createFoeTopRow(FoeActorType.ghost)) ;
    wait(2000) ;
    generateSingleEvent(createFoeTopRow(FoeActorType.health)) ;

    // Ghost + health wave
    wait(3000) ;
    generateFoeRandomTimed(2*60000, 500, 1500,
        createRandomFoe(
          [FoeActorType.bird, FoeActorType.ghost, FoeActorType.health],
          [null, null, null],
          [15, 5, 1]
        )) ;

    // Ink intro
    wait(2000) ;
    generateSingleEvent(createFoeTopRow(FoeActorType.ink)) ;
    wait(2000) ;
    generateSingleEvent(createFoeTopRow(FoeActorType.water)) ;

    // Ink + ghost + health
    wait(3000) ;
    generateFoeRandomTimed(2*60000, 500, 1500,
        createRandomFoe(
            [FoeActorType.bird, FoeActorType.ghost, FoeActorType.health,
              FoeActorType.ink, FoeActorType.water],
            [null, [1500, 3000], null, [1500, 5000], null],
            [10, 5, 1, 3, 2]
        )) ;

    // Some water
    regenBreak() ;

    // Missile intro
    wait(5000) ;
    generateFoeRandomNum(8, 1000, 3000,
        createSingleFoe(FoeActorType.missile, fadeIn: 3000)) ;

    // Repair break
    wait(2000) ;
    repairBreak() ;
    generateSingleEvent(createFoeTopRow(FoeActorType.bird)) ;

    // Missile intro round 2
    wait(5000) ;
    generateFoeRandomNum(4, 1000, 3000,
        createSingleFoe(FoeActorType.missile, fadeIn: 3000)) ;

    // Repair break
    wait(2000) ;
    repairBreak() ;
    generateSingleEvent(createFoeTopRow(FoeActorType.bird)) ;

    // All out round 1
    wait(3000) ;
    generateFoeRandomTimed(60000, 500, 1500,
        createRandomFoe(
            [FoeActorType.bird, FoeActorType.ghost, FoeActorType.health,
              FoeActorType.ink, FoeActorType.water, FoeActorType.missile,
              FoeActorType.seekingMissile],
            [null, [1500, 3000], null, [1500, 5000],
              null, [2000, 5000], [5000, 10000]],
            [25, 13, 3, 10, 6, 2, 1]
        )) ;

    // Great break
    wait(2000) ;
    repairBreak() ;
    regenBreak() ;
    generateSingleEvent(createFoeTopRow(FoeActorType.bird)) ;

    // All out round 2
    wait(3000) ;
    generateFoeRandomTimed(60000, 500, 1500,
        createRandomFoe(
            [FoeActorType.bird, FoeActorType.ghost, FoeActorType.health,
              FoeActorType.ink, FoeActorType.water, FoeActorType.missile,
              FoeActorType.seekingMissile],
            [null, [1500, 3000], null, [1500, 5000],
              null, [2000, 5000], [5000, 10000]],
            [25, 13, 3, 10, 6, 4, 2]
        )) ;

    // Great break
    wait(2000) ;
    repairBreak() ;
    regenBreak() ;
    generateSingleEvent(createFoeTopRow(FoeActorType.bird)) ;

    // All out round 3
    wait(3000) ;
    generateFoeRandomTimed(60000, 500, 1500,
        createRandomFoe(
            [FoeActorType.bird, FoeActorType.ghost, FoeActorType.health,
              FoeActorType.ink, FoeActorType.water, FoeActorType.missile,
              FoeActorType.seekingMissile],
            [null, [1500, 3000], null, [1500, 5000],
              null, [2000, 5000], [5000, 10000]],
            [25, 13, 3, 10, 6, 6, 3]
        )) ;

    wait(2000) ;
    generateSingleEvent(createFoeTopRow(FoeActorType.bird)) ;
    repairBreak() ;
    generateSingleEvent(createFoeTopRow(FoeActorType.bird)) ;
    wait(1000) ;
    generateSingleEvent(createFoeTopRow(FoeActorType.bird)) ;
    regenBreak() ;
    generateSingleEvent(createFoeTopRow(FoeActorType.bird)) ;
    wait(1000) ;
    generateSingleEvent(createFoeTopRow(FoeActorType.bird)) ;
    wait(1000) ;
    generateSingleEvent(createFoeTopRow(FoeActorType.bird)) ;
    generateSingleEvent(createSingleFoe(FoeActorType.seekingMissile, fadeIn: 6000)) ;
    generateSingleEvent(createSingleFoe(FoeActorType.seekingMissile, fadeIn: 6000)) ;
  }

  regenBreak() {
    generateFoeRandomTimed(8000, 500, 1500,
        createRandomFoe(
            [FoeActorType.bird, FoeActorType.water, FoeActorType.health],
            [null, null, null],
            [5, 5, 1]
        )) ;
  }

  repairBreak() {
    generateSingleEvent(createFoeTopRow(FoeActorType.bird)) ;
    generateFoeRandomTimed(25000, 100, 1100,
        createSingleFoe(FoeActorType.bird)) ;
  }

  ScenarioEvent createSingleFoe(FoeActorType type, {int? fadeIn}) {
    return (_) {
      FoeActor curFoe = getFoe(type)
        ..setRandomPosition() ;

      if (fadeIn != null) {
        curFoe.fadeIn = fadeIn ;
      }

      createFoe(curFoe) ;

      return true ;
    } ;
  }

  ScenarioEvent createFoeTopRow(FoeActorType foeType) {
    return (_) {
      for (int m = -3 ; m < 4 ; m += 2) {
        FoeActor curFoe = getFoe(foeType)
          ..setPosition([m, -5]) ;

        createFoe(curFoe) ;
      }

      return true ;
    } ;
  }

  ScenarioEvent createRandomFoe(
      List<FoeActorType> listFoeType,
      List<List<int>?> fadeInRange,
      List<int> weights
    ) {
    List<int> cumulativeWeight = List.filled(weights.length, 0) ;

    cumulativeWeight[0] = weights[0] ;

    for (int m = 1 ; m < weights.length ; m++) {
      cumulativeWeight[m] = cumulativeWeight[m-1] + weights[m] ;
    }

    return (_) {
      double foeSelector = Random().nextDouble() ;

      for (int m = 0 ; m < weights.length ; m++) {
        if (foeSelector <= cumulativeWeight[m]/cumulativeWeight.last) {
          FoeActor curFoe = getFoe(listFoeType[m])
            ..setRandomPosition() ;

          if (fadeInRange[m] != null) {
            curFoe.fadeIn = fadeInRange[m]![0] +
                Random().nextInt(fadeInRange[m]![1] -fadeInRange[m]![0] + 1) ;
          }

          createFoe(curFoe) ;
          break ;
        }
      }

      return true ;
    } ;
  }
}