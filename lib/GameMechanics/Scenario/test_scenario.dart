import '../Foe/foe_actor.dart' ;
import 'scenario.dart' ;

class ScenarioTest extends Scenario {

  ScenarioTest(Function(FoeActor) createFoe)
      : super(createFoe){
    wait(2000) ;
    generateFoeRandomTimed(60*1000, 2*1000, 4*1000, eventCreateFoeTest) ;
  }

  bool eventCreateFoeTest(int deltaTime) {
    FoeActor curFoe = getFoe(FoeActorType.missile)
      ..setRandomPosition() ;

    createFoe(curFoe) ;

    return true ;
  }
}
