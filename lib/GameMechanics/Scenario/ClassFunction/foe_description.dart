part of '../scenario.dart' ;

FoeActor _getFoe(FoeActorType foeType) {
  FoeActor curFoe = FoeActor() ;

  switch (foeType) {
    case FoeActorType.bird:
      curFoe
        ..actorGraphic = ActorGraphic.bird
        ..actorCollisionType = ActorCollisionType.none
        ..actorCollisionEffect = ActorCollisionEffect.none
        ..actorInitialTransform[0] = -1 ;
      break ;
    case FoeActorType.ghost:
      curFoe
        ..actorGraphic = ActorGraphic.ghost
        ..actorCollisionType = ActorCollisionType.collideCircle
        ..actorCollisionEffect = ActorCollisionEffect.hurtCircle
        ..actorInitialTransform = Matrix2.rotation(-pi/5)
        ..actorSize = 0.15 ;
      break ;
    case FoeActorType.missile:
      curFoe
        ..actorGraphic = ActorGraphic.rocket
        ..actorCollisionType = ActorCollisionType.collideAny
        ..actorCollisionEffect = ActorCollisionEffect.breakTile
        ..actorInitialTransform = Matrix2.rotation(pi/2)
        ..actorSize = 0.15 ;
      break ;
    case FoeActorType.seekingMissile:
      curFoe
        ..actorGraphic = ActorGraphic.nastyRocket
        ..actorCollisionType = ActorCollisionType.collideAny
        ..actorCollisionEffect = ActorCollisionEffect.breakTileNasty
        ..isSeeking = true
        ..actorInitialTransform = Matrix2.rotation(pi/5)
        ..actorSize = 0.15 ;
      break ;
    case FoeActorType.ink:
      curFoe
        ..actorGraphic = ActorGraphic.inkDroplet
        ..actorCollisionType = ActorCollisionType.collideAny
        ..actorCollisionEffect = ActorCollisionEffect.tileDirty
        ..actorInitialTransform = Matrix2.rotation(-pi/2) ;
      break ;
    case FoeActorType.water:
      curFoe
        ..actorGraphic = ActorGraphic.waterDroplet
        ..actorCollisionType = ActorCollisionType.collideAny
        ..actorCollisionEffect = ActorCollisionEffect.tileClean
        ..actorInitialTransform = Matrix2.rotation(-pi/2) ;
      break ;
    case FoeActorType.health:
      curFoe
        ..actorGraphic = ActorGraphic.heart
        ..actorCollisionType = ActorCollisionType.collideCircle
        ..actorCollisionEffect = ActorCollisionEffect.healCircle
        ..preventRotation = true
        ..actorSize = 0.13 ;
      break ;
  }

  return curFoe ;
}