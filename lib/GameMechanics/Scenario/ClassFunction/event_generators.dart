part of '../scenario.dart' ;

_wait(Scenario that, int waitDuration) {
  that.listFunc.add((deltaTime) {
    that.curTime += deltaTime ;
    if (that.curTime >= waitDuration) {
      that.curTime -= waitDuration ;
      return true ;
    }
    return false ;
  }) ;

  Counters.addTime(waitDuration) ;
}

_generateFoeRandomTimed(Scenario that, int attackDuration,
    int minWait, int maxWait,
    ScenarioEvent eventCreateFoe) {
  int waitDifference = maxWait - minWait ;
  int totalDuration = minWait ;
  int waitTime ;

  while (true) {
    waitTime = minWait + Random().nextInt(waitDifference) ;

    that.generateSingleEvent(eventCreateFoe) ;

    if (totalDuration >= attackDuration) {break ;}

    that.wait(waitTime) ;
    totalDuration += waitTime ;
  }

  waitTime = attackDuration + minWait - totalDuration ;
  that.wait(waitTime) ;
}

_generateFoeRandomNum(Scenario that, int numFoes,
    int minWait, int maxWait,
    ScenarioEvent eventCreateFoe) {
  int waitDifference = maxWait - minWait ;

  for (int m = 0 ; m < numFoes-1 ; m++) {
    that.generateSingleEvent(eventCreateFoe) ;
    that.wait(minWait + Random().nextInt(waitDifference)) ;
  }

  that.generateSingleEvent(eventCreateFoe) ;
}