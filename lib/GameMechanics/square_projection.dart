import 'package:vector_math/vector_math_64.dart' ;
import 'dart:math' ;
import 'PuzzleMechanics/SubFunctions/direction.dart' ;
import '../Common/constants.dart' as cst;

class SquareDim {
  static Vector2 centerPuzzle = Vector2(1.5, 1.5) ;
  static Vector2 centerUi = Vector2(0, 0) ;
  static double halfLengthPuzzle = 2*cst.spawnLine ;
  static double halfLengthUi = cst.spawnLine ;
}

Vector2 generateRandomPositionSquarePuzzle() {
  Vector2 normPosition = getPositionOnSquare(
      Random().nextInt(4),
      2*Random().nextDouble() - 1,
      false
  ) ;

  return normPosition*SquareDim.halfLengthPuzzle + SquareDim.centerPuzzle ;
}

Vector2 generateRandomPositionSquareDiscreteUi() {
  List<double> paramPos = [-1, -0.75/cst.spawnLine, -0.25/cst.spawnLine,
    0.25/cst.spawnLine, 0.75/cst.spawnLine] ;

  Vector2 normPosition = getPositionOnSquare(
      Random().nextInt(4),
      paramPos[Random().nextInt(paramPos.length)],
      false
  ) ;

  return normPosition*SquareDim.halfLengthUi + SquareDim.centerUi ;
}

Vector2 projectToSquarePuzzle(Vector2 position) {
  Vector2 deltaPos = position - SquareDim.centerPuzzle ;
  Vector2 normPosition = getPositionOnSquare(
      Direction.getCurrentDirection(deltaPos),
      deltaPos.x.abs() > deltaPos.y.abs() ?
        deltaPos.y / deltaPos.x : deltaPos.x / deltaPos.y,
      true
  ) ;

  return normPosition*SquareDim.halfLengthPuzzle + SquareDim.centerPuzzle ;
}

Vector2 getPositionOnSquare(int direction, double param, bool applySign) {
  int negSign = applySign ? -1 : 1 ;

  if (param.isNaN) {
    param = 0 ;
  }

  switch (direction) {
    case 0: return Vector2(1, param) ;
    case 1: return Vector2(param, 1) ;
    case 2: return Vector2(-1, negSign*param) ;
    case 3: return Vector2(negSign*param, -1) ;
  }

  return Vector2(0, 0) ;
}