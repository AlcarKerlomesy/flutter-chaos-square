import 'PuzzleMechanics/puzzle.dart';
import '../Common/callback_typedef.dart';
import '../AnimationInterface/update_menu.dart' ;
import 'Foe/foe_master.dart' ;
import 'counters.dart' ;

enum GameMode {
  none,
  classic,
  random,
  chaos,
  chaosSkipIntro,
  hideAndSeek, // phase 1 of chaos
  foeWaves, // phase 2 of chaos
}

class GameMaster {
  static bool showMenu = true ;
  static MenuSelect menuSelect = MenuSelect.startMenu ;
  static bool isPaused = false ;
  static GameMode gameMode = GameMode.none ;
  static Callback? menuRedraw ;

  static init() {
    Puzzle.hideTiles(true) ;
    Puzzle.announceWin = winGame ;
    Puzzle.announceLost = loseGame ;
  }

  static continueGame() {
    menuSelect = MenuSelect.none ;
    Puzzle.hideTiles(false) ;
    FoeMaster.startTimer() ;
    menuRedraw?.call() ;
  }

  static pauseGame() {
    menuSelect = MenuSelect.pauseMenu ;
    Puzzle.hideTiles(true) ;
    FoeMaster.stopTimer() ;
    menuRedraw?.call() ;
  }

  static startGame(GameMode startGameMode) {
    menuSelect = MenuSelect.none ;
    gameMode = startGameMode ;

    Counters.resetMoveCounter() ;

    switch (startGameMode) {
      case GameMode.classic:
        Puzzle.initState(true, false) ; break ;
      case GameMode.random:
        Puzzle.initState(null, true, 15) ; break ;
      case GameMode.chaos:
        Puzzle.initState(true, true, 4) ;
        gameMode = GameMode.hideAndSeek ;
        break ;
      case GameMode.chaosSkipIntro:
        Puzzle.initState(true, true, 4) ;
        gameMode = GameMode.foeWaves ;
        Puzzle.bringCircleToFront() ;
        FoeMaster.launchAttack() ;
        Counters.addMoveCounter(100) ;
        break ;
      default:
    }

    Puzzle.hideTiles(false) ;
    menuRedraw?.call() ;
  }

  static abortGame() {
    menuSelect = MenuSelect.startMenu ;
    Puzzle.hideTiles(true) ;
    FoeMaster.endAttack() ;
    menuRedraw?.call() ;
  }

  static winGame() {
    switch (gameMode) {
      case (GameMode.hideAndSeek):
        gameMode = GameMode.foeWaves ;
        Puzzle.bringCircleToFront() ;
        FoeMaster.launchAttack() ;
        return ;
      case (GameMode.foeWaves):
        if (FoeMaster.foesIsAttacking) {return ;}
        break ;
      default:
    }
    menuSelect = MenuSelect.winMenu ;
    menuRedraw?.call() ;
  }

  static loseGame() {
    menuSelect = MenuSelect.loseMenu ;
    FoeMaster.stopTimer() ;
    menuRedraw?.call() ;
  }
}