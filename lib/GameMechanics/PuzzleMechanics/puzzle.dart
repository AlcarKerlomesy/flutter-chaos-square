import 'dart:async' ;
import 'package:vector_math/vector_math_64.dart' ;
import '../Foe/foe_actor.dart';
import '../square_projection.dart' ;
import '../../Common/coordinate_transform.dart';
import '../../Common/callback_typedef.dart';
import '../../Common/constants.dart' as cst ;

import 'tile_state.dart' ;
import 'circle_state.dart' ;
import 'SubFunctions/make_puzzle_solvable.dart' ;
import 'SubFunctions/puzzle_movement.dart' ;
import 'SubFunctions/win_condition.dart' ;

part 'ClassFunctions/initialization.dart' ;
part 'ClassFunctions/pointer.dart' ;
part 'ClassFunctions/collision.dart' ;
part 'ClassFunctions/pointer_broken_piece.dart' ;

Vector2 indexToPosition(int index) {
  double row = (index/4).floor().toDouble() ;
  return Vector2(index - 4*row, row) ;
}

int positionToIndex(Vector2 position) {
  return 4*position[1].clamp(0, 3).round()
      + position[0].clamp(0, 3).round() ;
}

bool isInsideBoard(Vector2 position) {
  return (position[0] >= -0.5 && position[0] < 3.5 &&
      position[1] >= -0.5 && position[1] < 3.5) ;
}

class Puzzle {
  static List<int> puzzleState =
      List.filled(cst.numTilePosition, 0, growable: false) ;
  static List<TileState> tilesState = List.generate(
      cst.listTileLength,
      (index) => TileState(index, Vector2(0, 0)), growable: false) ;
  static bool isHidden = false ;

  static Callback? redrawPuzzleVisibility ;
  static Callback? redrawCirclePatch ;
  static Callback? redrawPieceStack ;
  static Callback? announceWin ;
  static Callback? announceLost ;

  static bool isSelected = false ;
  static int selectedTile = 0 ;
  static bool computeAllowedDirection = false ;
  static List<bool> allowedDirection = List.filled(4, false, growable: false) ;

  static List<List<int>> secretCircles = [] ;

  static List<TileState> tilePiecesState = List.generate(
      cst.listPieceLength,
      (index) => TileState((index ~/ 3) + 1, Vector2(0, 0))..explodedState = 1,
      growable: false) ;
  static int selectedPiece = -1 ;

  static initState(bool? isSolvable, bool generateCircle, [int numCircle = 0]) {
    _initState(isSolvable, generateCircle, numCircle) ;
  }

  static bringCircleToFront() {
    _bringCircleToFront() ;
  }

  static hideTiles(bool doHide) {
    if (doHide != isHidden) {
      isHidden = doHide ;
      redrawPuzzleVisibility?.call() ;
    }
  }

  static CircleState getCircleState(int index) {
    return CircleState(
        secretCircles[index][1],
        puzzleToUIPosition(indexToPosition(secretCircles[index][0]))
    ) ;
  }

  static pointerDown(Vector2 pointerPos) {
    _pointerDown(pointerPos) ;
  }

  static bool pointerMove(Vector2 pointerPos) {
    return _pointerMove(pointerPos) ;
  }

  static pointerUp() {
    _pointerUp() ;
  }

  static bool checkCollision(
      Vector2 position,
      ActorCollisionType collisionType,
      ActorCollisionEffect collisionEffect) {
    return _checkCollision(position, collisionType, collisionEffect) ;
  }

  static bool pointerDownBrokenPiece(Vector2 pointerPos) {
    return _pointerDownBrokenPiece(pointerPos) ;
  }

  static pointerMoveBrokenPiece(Vector2 pointerPos) {
    _pointerMoveBrokenPiece(pointerPos) ;
  }

  static pointerUpBrokenPiece() {
    _pointerUpBrokenPiece() ;
  }

  static checkWinStandard() {
    if (
      Puzzle.tilePiecesState.every((element) => element.explodedState == 1) &&
      isStandardWin(Puzzle.puzzleState)
    ) {Puzzle.announceWin?.call() ;}
  }
}