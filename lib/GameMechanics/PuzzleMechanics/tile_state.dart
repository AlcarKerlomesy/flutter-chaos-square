import 'package:vector_math/vector_math_64.dart' ;
import '../../Common/callback_typedef.dart';

class TileState {
  int number ;

  int circleState = 0 ; // 0: no circle, 1: dead, 2: hurt, 3: healthy
  int dirtyness = 0 ; // 0: clean, 1: dirty, 2: covered

  // 0: not exploded, 1: completely exploded,
  // 2, 3, 5: Multiply this number for each piece
  // 2, 3, 5, 6, 10, 15: Some pieces are gathered
  // 30: Cracked
  int explodedState = 0 ;

  Vector2 position ;

  Callback? redrawTileVisibility ;
  Callback? redrawTilePosition ;
  Callback? redrawTileState ;

  TileState(this.number, this.position) ;

  copyContentTo(TileState target) {
    target.number = number ;
    target.circleState = circleState ;
    target.dirtyness = dirtyness ;
    target.explodedState = explodedState ;
  }

  TileState clone() {
    TileState newTile = TileState(number, position) ;
    copyContentTo(newTile) ;
    return newTile ;
  }

  resetState() {
    circleState = 0 ;
    dirtyness = 0 ;
    explodedState = 0 ;
  }

  redrawTile() {
    redrawTileState?.call() ;
    redrawTilePosition?.call() ;
    redrawTileVisibility?.call() ;
  }
}