bool isStandardWin(List<int> puzzleState) {

  for (int m = 0 ; m < 15 ; m++) {
    if (puzzleState[m] != m + 1) {
      return false ;
    }
  }

  return true ;
}

bool isCircleWin(List<List<int>> circles, List<int> puzzleState) {

  for (int m = 0 ; m < circles.length ; m++) {
    if (circles[m][1] != puzzleState[circles[m][0]]) {
      return false ;
    }
  }

  return true ;
}