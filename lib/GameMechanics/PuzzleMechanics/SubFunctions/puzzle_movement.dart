import 'package:vector_math/vector_math_64.dart' ;
import 'direction.dart';
import '../../counters.dart' ;

List<bool> getAllowedDirection(List<int> puzzleState, int selectedTile) {
  List<bool> allowedDirection = List.filled(4, false) ;

  for (int m = 0 ; m < 4 ; m++) {
    if (Direction.getIndexNextEmptySlot(
          selectedTile, puzzleState,
          Direction.getDirection(m)) != null
    ) {
      allowedDirection[m] = true ;
    }
  }

  return allowedDirection ;
}

class DeltaOutput {
  Vector2 tileDelta ;
  Vector2 directionVector ;
  bool isOutOfBound ;
  bool moveTiles ;

  DeltaOutput(this.tileDelta, this.directionVector, this.isOutOfBound, this.moveTiles) ;
}

DeltaOutput getDeltaPosition(Vector2 tilePos,
    List<bool> allowedDirection, Vector2 pointerPos) {
  Vector2 pointerDelta = pointerPos - tilePos ;
  Vector2 tileDelta = Vector2(0, 0) ;

  bool moveTiles = false, isOutOfBound = false ;
  int currentDir = Direction.getCurrentDirection(pointerDelta) ;
  Vector2 directionVector = Direction.getDirection(currentDir) ;
  double projFactor = pointerDelta.dot(directionVector) ;

  if (projFactor > 1.0) {
    isOutOfBound = true ;
  } else if (allowedDirection[currentDir]) {
    tileDelta = directionVector*projFactor ;
    if (projFactor > 0.5) {moveTiles = true ;}
  } else {
    int leftDirection = (currentDir - 1) % 4 ;
    int rightDirection = (currentDir + 1) % 4 ;

    if (allowedDirection[leftDirection] || allowedDirection[rightDirection]) {
      directionVector = Direction.getDirection(rightDirection) ;
      projFactor = pointerDelta.dot(directionVector) ;

      if ((projFactor < 0 && allowedDirection[leftDirection]) ||
          (projFactor > 0 && allowedDirection[rightDirection])) {
        tileDelta = directionVector*projFactor ;
      }
    }
  }

  return DeltaOutput(tileDelta, directionVector, isOutOfBound, moveTiles) ;
}

int updatePuzzle(int Function(int, int) moveTile,
    int selectedTile, Vector2 moveDir) {
  int curIndex = selectedTile ;
  int newSelectedTile ;
  int curTile = moveTile(curIndex, 0) ;

  curIndex += Direction.getIndexVariation(moveDir) ;
  newSelectedTile = curIndex ;

  curTile = moveTile(curIndex, curTile) ;

  while (curTile != 0) {
    curIndex += Direction.getIndexVariation(moveDir) ;
    curTile = moveTile(curIndex, curTile) ;
  }

  Counters.addMoveCounter(1) ;

  return newSelectedTile ;
}