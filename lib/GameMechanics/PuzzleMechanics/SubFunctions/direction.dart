import 'package:vector_math/vector_math_64.dart' ;

class Direction {
  static Vector2 none = Vector2(0, 0) ;
  static Vector2 right = Vector2(1, 0) ;
  static Vector2 down = Vector2(0, 1) ;
  static Vector2 left = Vector2(-1, 0) ;
  static Vector2 up = Vector2(0, -1) ;

  static Vector2 getDirection(int index){
    switch (index) {
      case -1: return none ;
      case 0: return right ;
      case 1: return down ;
      case 2: return left ;
      case 3: return up ;
    }

    return none ;
  }

  static int getIndexVariation(Vector2 direction) {
    return (4*direction.y + direction.x).round() ;
  }

  static int getCurrentDirection(Vector2 deltaPos) {
    if (deltaPos.x.abs() < deltaPos.y.abs()) {
      return deltaPos.y < 0 ? 3 : 1 ;
    } else {
      return deltaPos.x < 0 ? 2 : 0 ;
    }
  }

  static int? getIndexNextEmptySlot(int index, List<int> puzzleState, Vector2 direction) {
    int deltaIndex = getIndexVariation(direction) ;
    int firstInRow = (index ~/ 4) * 4 ;
    int lastIndex ;

    if (direction.x == 0) {
      lastIndex = (direction.y > 0 ? 12 : 0) + index - firstInRow ;
    } else {
      lastIndex = firstInRow + (direction.x > 0 ? 3 : 0) ;
    }

    if (index == lastIndex) { return null ; }

    do {
      index += deltaIndex ;
      if (puzzleState[index] == 0) {
        return index ;
      }
    } while (index != lastIndex) ;

    return null ;
  }
}