makePuzzleSolvable(List<int> puzzleState, bool makeItSolvable) {
  PuzzleSolvableOutput output = isPuzzleSolvable(puzzleState) ;

  if (output.isSolvable ^ makeItSolvable) {
    puzzleState[output.pos14] = 15 ;
    puzzleState[output.pos15] = 14 ;
  }
}

makePuzzleSolvableRandom(List<int> puzzleState, List<List<int>> listCircles) {

  if (listCircles.length == 15) {
    List<int> circlesState = List.filled(16, 0) ;
    PuzzleSolvableOutput output1 = isPuzzleSolvable(puzzleState) ;
    PuzzleSolvableOutput output2 ;

    for (int m = 0 ; m < listCircles.length ; m++) {
      circlesState[listCircles[m][0]] = listCircles[m][1] ;
    }

    output2 = isPuzzleSolvable(circlesState) ;

    if (output1.isSolvable ^ output2.isSolvable) {
      puzzleState[output1.pos14] = 15 ;
      puzzleState[output1.pos15] = 14 ;
    }
  }

}

class PuzzleSolvableOutput {
  int pos14 ;
  int pos15 ;
  bool isSolvable ;

  PuzzleSolvableOutput(this.pos14, this.pos15, this.isSolvable) ;
}

isPuzzleSolvable(List<int> tilesState) {
  int pos14 = -1, pos15 = -1, index ;
  bool isSolvable = false ;
  // The reason why isSolvable is false here is tricky to explain. Hint:
  // Here, the puzzle is assumed solved if the empty square is at the
  // top-left corner

  for (int m1 = 0 ; m1 < 4 ; m1++) {
    for (int m2 = 0 ; m2 < 4 ; m2++) {
      index = 4*m1+m2 ;

      switch (tilesState[index]) {
        case 0:
        // Taxicab distance
          isSolvable ^= (m1 + m2).isOdd ;
          break ;
        case 15:
          pos15 = index ;
          // equivalent to (15 - index).isOdd
          isSolvable ^= index.isEven ;
          break ;
        case 14:
          pos14 = index;
          // equivalent to (15 - index - (pos15 == -1 ? 1 : 0)).isOdd
          isSolvable ^= (index.isEven ^ (pos15 == -1)) ;
          break ;
        default:
          for (int m3 = index+1 ; m3 < 16 ; m3++) {
            isSolvable ^= tilesState[index] > tilesState[m3] ;
          }
      }
    }
  }

  return PuzzleSolvableOutput(pos14, pos15, isSolvable) ;
}