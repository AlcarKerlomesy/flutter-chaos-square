part of '../puzzle.dart' ;

_initState(bool? isSolvable, bool generateCircle, int numCircle) {
  for (int m = 0; m < 16; m++) {
    Puzzle.puzzleState[m] = m;
  }
  Puzzle.puzzleState.shuffle();

  if (generateCircle) {
    Puzzle.secretCircles = initSecretCircles(numCircle) ;
  } else {
    Puzzle.secretCircles = [] ;
  }

  if (isSolvable == null) {
    makePuzzleSolvableRandom(Puzzle.puzzleState, Puzzle.secretCircles);
  } else {
    makePuzzleSolvable(Puzzle.puzzleState, isSolvable) ;
  }

  for (int m = 0 ; m < cst.numTilePosition ; m++) {
    if (Puzzle.puzzleState[m] > 0) {
      TileState curTile = Puzzle.tilesState[Puzzle.puzzleState[m]] ;
      curTile.position = indexToPosition(m) ;
      curTile.resetState() ;
      curTile.redrawTile() ;
    }
  }

  for (int m = 0 ; m < cst.listPieceLength ; m++) {
    Puzzle.tilePiecesState[m].explodedState = 1 ;
    Puzzle.tilePiecesState[m].redrawTileVisibility?.call() ;
  }

  Puzzle.redrawCirclePatch?.call() ;
}

_bringCircleToFront() {
  for (int m = 0 ; m < Puzzle.secretCircles.length ; m++) {
    TileState curTile = Puzzle.tilesState[Puzzle.secretCircles[m][1]] ;
    curTile.circleState = 3 ;
    curTile.redrawTileState?.call() ;
  }

  Puzzle.secretCircles = [] ;

  Puzzle.redrawCirclePatch?.call() ;
}

List<List<int>> initSecretCircles(int numCircle) {
  List<int> rndList = List.generate(16, (i) => i) ;
  List<List<int>> secretCircles = List.filled(numCircle, [0, 0]) ;

  rndList.shuffle() ;
  for (int m = 0 ; m < numCircle ; m++) {
    secretCircles[m] = [rndList[m], 0] ;
  }

  rndList = List.generate(15, (i) => i+1) ;
  rndList.shuffle() ;
  for (int m = 0 ; m < numCircle ; m++) {
    secretCircles[m][1] = rndList[m] ;
  }

  return secretCircles ;
}