part of '../puzzle.dart';

bool _checkCollision(
    Vector2 position,
    ActorCollisionType collisionType,
    ActorCollisionEffect collisionEffect
    ) {
  Vector2 roundedPosition = position.clone()..round();
  bool doCollide = false ;
  bool doDestroy = false ;
  int tileNumber;

  if (isInsideBoard(position)) {
    if ((position.x - roundedPosition.x).abs() <= 0.49 &&
        (position.y - roundedPosition.y).abs() <= 0.49 &&
        (tileNumber = Puzzle.puzzleState[positionToIndex(roundedPosition)]) >
            0) {
      switch (collisionType) {
        case ActorCollisionType.none:
          break ;
        case ActorCollisionType.collideAny:
          if (Puzzle.tilesState[tileNumber].explodedState == 0 ||
              Puzzle.tilesState[tileNumber].explodedState == 30) {
            doCollide = true ;
          }
          break;
        case ActorCollisionType.collideCircle:
          if (Puzzle.tilesState[tileNumber].circleState > 0) {
            doCollide = true ;
          }
          break;
      }

      if (doCollide) {
        applyCollisionEffect(
            Puzzle.tilesState[tileNumber],
            collisionEffect
        );
        doDestroy = true ;
      }
    }
  }

  return doDestroy;
}

applyCollisionEffect(TileState tile, ActorCollisionEffect collisionEffect) {
  switch (collisionEffect) {
    case ActorCollisionEffect.none:
      break ;
    case ActorCollisionEffect.breakTile:
      if (tile.explodedState == 0) {
        tile.explodedState = 30 ;
        tile.redrawTileState?.call() ;
      } else if (tile.explodedState == 30) {
        explodeTile(tile) ;
        if (tile.circleState > 0) {
          Puzzle.announceLost?.call() ;
        }
      }
      break ;
    case ActorCollisionEffect.breakTileNasty:
      if (tile.explodedState == 0 || tile.explodedState == 30) {
        explodeTile(tile) ;
        if (tile.circleState > 0) {
          Puzzle.announceLost?.call() ;
        }
      }
      break ;
    case ActorCollisionEffect.hurtCircle:
      if (tile.circleState > 1) {
        tile.circleState-- ;
        tile.redrawTileState?.call() ;
        if (tile.circleState == 1) {
          Puzzle.announceLost?.call() ;
        }
      }
      break ;
    case ActorCollisionEffect.healCircle:
      if (tile.circleState == 2) {
        tile.circleState++ ;
        tile.redrawTileState?.call() ;
      }
      break ;
    case ActorCollisionEffect.tileDirty:
      if (tile.dirtyness != 2) {
        tile.dirtyness = 2 ;
        tile.redrawTileState?.call() ;
      }
      break ;
    case ActorCollisionEffect.tileClean:
      if (tile.dirtyness > 0) {
        tile.dirtyness-- ;
        tile.redrawTileState?.call() ;
      }
      break ;
  }
}

explodeTile(TileState tile) {
  List<int> explodedState = [2, 3, 5] ;
  int index = 3*(tile.number - 1) ;

  tile.explodedState = 1 ;
  Puzzle.puzzleState[positionToIndex(tile.position)] = 0 ;
  Puzzle.computeAllowedDirection = true ;

  tile.redrawTileState?.call() ;
  tile.redrawTileVisibility?.call() ;

  for (int m = 0 ; m < 3 ; m++) {
    tile.copyContentTo(Puzzle.tilePiecesState[index]) ;
    Puzzle.tilePiecesState[index].position = tile.position ;
    Puzzle.tilePiecesState[index].explodedState = explodedState[m] ;
    Puzzle.tilePiecesState[index].redrawTile() ;
    index++ ;
  }

  Timer(const Duration(milliseconds: 50), () {
    for (int m = 3*(tile.number - 1) ; m < 3*tile.number ; m++) {
      Puzzle.tilePiecesState[m].position = generateRandomPositionSquarePuzzle() ;
      Puzzle.tilePiecesState[m].redrawTilePosition?.call() ;
    }
  }) ;
}