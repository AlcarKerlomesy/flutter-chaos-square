part of '../puzzle.dart';

bool _pointerDownBrokenPiece(Vector2 pointerPos) {
  bool isPieceSelected = false;

  for (int m = cst.listPieceLength - 1; m >= 0; m--) {
    if (Puzzle.tilePiecesState[m].explodedState != 1) {
      if ((pointerPos.x - Puzzle.tilePiecesState[m].position.x).abs() <= 0.5 &&
          (pointerPos.y - Puzzle.tilePiecesState[m].position.y).abs() <= 0.5) {
        Puzzle.selectedPiece = m;
        isPieceSelected = true;
        break;
      }
    }
  }

  if (isPieceSelected) {
    Puzzle.pointerMoveBrokenPiece(pointerPos);
  }

  return isPieceSelected;
}

_pointerMoveBrokenPiece(Vector2 pointerPos) {
  Puzzle.tilePiecesState[Puzzle.selectedPiece].position = pointerPos;
  Puzzle.tilePiecesState[Puzzle.selectedPiece].redrawTilePosition?.call();
}

_pointerUpBrokenPiece() {
  bool goBackToInitialPosition = true;
  TileState brokenPiece = Puzzle.tilePiecesState[Puzzle.selectedPiece];
  TileState reconstructedTile = Puzzle.tilesState[brokenPiece.number] ;
  int index = positionToIndex(brokenPiece.position);

  if (isInsideBoard(brokenPiece.position)) {

    if (Puzzle.tilesState[brokenPiece.number].explodedState == 1) {
      if (Puzzle.puzzleState[index] == 0) {
        Puzzle.puzzleState[index] = brokenPiece.number;
        reconstructedTile.position = indexToPosition(index);
        goBackToInitialPosition = false;
      }
    } else if (Puzzle.puzzleState[index] == brokenPiece.number) {
      goBackToInitialPosition = false;
    }
  }

  if (goBackToInitialPosition) {
    brokenPiece.position = projectToSquarePuzzle(brokenPiece.position);
    brokenPiece.redrawTilePosition?.call();
  } else {
    reconstructedTile.explodedState *= brokenPiece.explodedState;
    if (reconstructedTile.explodedState < 6) {
      reconstructedTile.redrawTile() ;
    } else {
      reconstructedTile.redrawTileState?.call();
    }

    brokenPiece.explodedState = 1 ;
    brokenPiece.redrawTileVisibility?.call();

    if (reconstructedTile.explodedState == 30 &&
        brokenPiece.number == index + 1) {
      Puzzle.checkWinStandard() ;
    }
  }

  Puzzle.selectedPiece = -1;
}
