part of '../puzzle.dart' ;

_pointerDown(Vector2 pointerPos) {
  Puzzle.isSelected = true ;
  Puzzle.selectedTile = positionToIndex(pointerPos) ;
  Puzzle.tilesState[Puzzle.puzzleState[Puzzle.selectedTile]]
      .copyContentTo(Puzzle.tilesState[0]) ;
  Puzzle.computeAllowedDirection = true ;

  _pointerMove(pointerPos) ;
  Puzzle.tilesState[0].redrawTileState?.call() ;
  Puzzle.tilesState[0].redrawTileVisibility?.call() ;
}

bool _pointerMove(Vector2 pointerPos) {
  DeltaOutput deltaOutput ;

  if (Puzzle.computeAllowedDirection) {
    Puzzle.allowedDirection = getAllowedDirection(
        Puzzle.puzzleState,
        Puzzle.selectedTile
    ) ;
    Puzzle.computeAllowedDirection = false ;
  }

  Puzzle.tilesState[0].position = indexToPosition(Puzzle.selectedTile) ;
  deltaOutput = getDeltaPosition(
      Puzzle.tilesState[0].position,
      Puzzle.allowedDirection,
      pointerPos
  ) ;

  if (!deltaOutput.isOutOfBound) {
    Puzzle.tilesState[0].position += deltaOutput.tileDelta ;
  }

  if (deltaOutput.moveTiles) {
    Puzzle.selectedTile = updatePuzzle(
        moveTile,
        Puzzle.selectedTile,
        deltaOutput.directionVector
    ) ;
    Puzzle.allowedDirection = getAllowedDirection(
        Puzzle.puzzleState,
        Puzzle.selectedTile
    ) ;
  }

  Puzzle.tilesState[0].redrawTilePosition?.call() ;

  return deltaOutput.isOutOfBound ;
}

_pointerUp() {
  Puzzle.isSelected = false ;
  Puzzle.tilesState[0].redrawTileVisibility?.call() ;

  // Check if it is a winning position
  if (Puzzle.secretCircles.isNotEmpty) {
    if (isCircleWin(Puzzle.secretCircles, Puzzle.puzzleState)) {
      Puzzle.announceWin?.call() ;
    }
  } else if (Puzzle.puzzleState[Puzzle.selectedTile] == Puzzle.selectedTile + 1) {
    Puzzle.checkWinStandard();
  }
}

int moveTile(int index, int tileNumber) {
  int removedTile = Puzzle.puzzleState[index] ;

  Puzzle.puzzleState[index] = tileNumber ;
  if (tileNumber > 0) {
    Puzzle.tilesState[tileNumber].position = indexToPosition(index);
    Puzzle.tilesState[tileNumber].redrawTilePosition?.call() ;
  }

  return removedTile ;
}