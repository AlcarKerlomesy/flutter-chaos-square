import 'package:flutter/scheduler.dart' ;
import '../Scenario/scenario.dart';
import '../PuzzleMechanics/puzzle.dart';
import '../counters.dart' ;
import '../../Common/callback_typedef.dart';
import 'foe_actor.dart' ;

class FoeMaster {
  static Callback? redrawFoes ;
  static Callback? redrawBackground ;

  static Ticker mainTicker = Ticker(gameTick) ;
  static int previousTime = 0 ;

  static bool foesIsAttacking = false ;
  static List<FoeActor> listFoes = [] ;
  static Scenario? scenario ;

  static launchAttack() {
    foesIsAttacking = true ;
    scenario = ScenarioNormal(createFoe) ;
    Counters.showTimer(true) ;
    startTimer() ;

    redrawBackground?.call() ;
  }

  static endAttack() {
    foesIsAttacking = false ;
    scenario = null ;
    listFoes = [] ;
    Counters.showTimer(false) ;
    stopTimer() ;

    redrawFoes?.call() ;
    redrawBackground?.call() ;
  }

  static startTimer() {
    if (foesIsAttacking) {
      mainTicker.start() ;
      previousTime = 0 ;
    }
  }

  static stopTimer() {
    mainTicker.stop() ;
  }

  static createFoe(FoeActor addedFoe) {
    listFoes.add(addedFoe) ;
  }

  static gameTick(Duration elapsedTime) {
    int index = 0 ;
    bool scenarioEnded = false ;
    int deltaTime = (elapsedTime.inMilliseconds - previousTime).clamp(0, 200) ;
    previousTime = elapsedTime.inMilliseconds ;

    Counters.addTime(-deltaTime) ;

    if (scenario != null) {
      scenarioEnded = scenario!.nextAction(deltaTime) ;
    }

    for (int m = 0 ; m < listFoes.length ; m++) {
      if (listFoes[index].updateFoe(deltaTime)) {
        listFoes.removeAt(index) ;
      } else {index++ ;}
    }

    if (scenarioEnded && listFoes.isEmpty) {
      endAttack() ;
      Puzzle.checkWinStandard() ;
    }

    redrawFoes?.call() ;
  }
}