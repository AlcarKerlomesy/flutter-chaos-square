import 'package:vector_math/vector_math_64.dart' ;
import 'dart:math' ;
import '../../Common/constants.dart' as cst;
import '../../Common/coordinate_transform.dart';
import '../PuzzleMechanics/puzzle.dart';
import '../square_projection.dart' ;

class FoeActor {
  Vector2 position = Vector2(0, 0) ;
  double maxLimit = cst.spawnLine ;

  Vector2 velocityDirection = Vector2(0, 0) ; // [-1, 0, 1]
  double velocity = cst.defaultVelocity ; // in tile per second
  double maxAngleVariation = cst.defaultMaxAngleVariation ; // in degrees per second

  bool isSeeking = false ;
  int indexTargetTile = 0 ;

  int fadeIn = cst.defaultFadeIn ; // in ms
  int fadeOut = cst.defaultFadeOut ;

  late double maxActorSize ;
  double actorSizeDiff = cst.defaultActorSizeDiff ;
  double actorSize = cst.defaultActorSize ;

  Matrix2 actorInitialTransform = Matrix2.identity() ;
  Matrix2 actorTransform = Matrix2.identity() ;
  bool preventRotation = false ;

  ActorGraphic actorGraphic = ActorGraphic.bird ;
  ActorCollisionType actorCollisionType = ActorCollisionType.none ;
  ActorCollisionEffect actorCollisionEffect = ActorCollisionEffect.none ;

  ActorActionPhase actionPhase = ActorActionPhase.appearing ;

  double opacity = 0 ;
  int internalOpacity = 0 ;

  FoeActor() ;

  setRandomPosition() {
    position = generateRandomPositionSquareDiscreteUi() ;
  }

  setPosition(List<int> startPos) {
    position = Vector2(startPos[0]/4, startPos[1]/4) ;
    if (startPos[0].abs() == 5) {
      position.x = startPos[0]*cst.spawnLine/5 ;
    }
    if (startPos[1].abs() == 5) {
      position.y = startPos[1]*cst.spawnLine/5 ;
    }
  }

  Vector2 getTransformPosition() {
    Matrix2 tempMatrix = Matrix2.identity() - actorTransform*actorSize
        ..invert() ;

    return tempMatrix*position ;
  }

  Matrix4 getTransformMatrix() {
    Matrix4 tempMatrix = Matrix4.identity()
        ..setUpper2x2(actorTransform*actorSize) ;

    return tempMatrix ;
  }

  setTransformMatrix() {
    if (preventRotation) {
      actorTransform =
          Matrix2(velocityDirection.x < 0 ? -1 : 1, 0, 0, 1)
              * actorInitialTransform ;
    } else {
      actorTransform =
          Matrix2.rotation(atan2(velocityDirection.y, velocityDirection.x))
          * Matrix2(1, 0, 0, velocityDirection.x < 0 ? -1 : 1)
          * actorInitialTransform ;
    }
  }

  Vector2 getTargetDirection() {
    return puzzleToUIPosition(
        Puzzle.tilesState[indexTargetTile].position
    ) - position ;
  }

  bool updateFoe(int deltaTime) {
    switch (actionPhase) {
      case ActorActionPhase.appearing:
        if (opacity == 0) { // initialization
          maxActorSize = actorSize + actorSizeDiff ;
          actorSize = maxActorSize ;

          if (isSeeking) {
            int circleId = Random().nextInt(4) ;
            int circleCount = 0 ;

            for (int m = 0 ; m < Puzzle.tilesState.length ; m++) {
              if (Puzzle.tilesState[m].circleState > 0) {
                if (circleCount == circleId) {
                  indexTargetTile = m ;
                  break ;
                } else {
                  circleCount++ ;
                }
              }
            }

            velocityDirection = getTargetDirection().normalized() ;
          } else {
            velocityDirection = - position / cst.spawnLine
              ..roundToZero() ;
          }

          setTransformMatrix() ;
        }

        internalOpacity += deltaTime ;
        opacity = (internalOpacity/fadeIn).clamp(0, 1) ;
        if (opacity == 1) {
          opacity = 1 ;
          actionPhase = ActorActionPhase.moving ;
        }

        actorSize = maxActorSize - opacity*actorSizeDiff ;
        break ;

      case ActorActionPhase.moving:
        if (isSeeking) {
          Vector2 deltaTarget = getTargetDirection() ;
          double angleFactor = deltaTime*pi/180/1000 ;

          if (velocityDirection.angleTo(deltaTarget)
              > maxAngleVariation*angleFactor) {
            velocityDirection = Matrix2.rotation(
                (velocityDirection.cross(deltaTarget) > 0 ? 1 : -1)
                *maxAngleVariation*angleFactor)*velocityDirection ;
          } else {
            velocityDirection = deltaTarget.normalized() ;
          }

          setTransformMatrix() ;
        }

        position = position
            + velocityDirection*deltaTime.toDouble()*(velocity/2000) ;

        if (Puzzle.checkCollision(uiToPuzzlePosition(position),
            actorCollisionType, actorCollisionEffect) ||
            (!isSeeking &&
                (position.x.abs() > maxLimit || position.y.abs() > maxLimit))) {
          actionPhase = ActorActionPhase.disappearing ;
          internalOpacity = fadeOut ;
        }

        break ;

      case ActorActionPhase.disappearing:
        internalOpacity -= deltaTime ;
        opacity = (internalOpacity/fadeOut).clamp(0, 1) ;
        if (opacity == 0) {
          opacity = 0 ;
          return true ;
        }

        actorSize = maxActorSize - opacity*actorSizeDiff ;
        break ;
    }

    return false ;
  }
}

enum ActorGraphic {
  bird,
  ghost,
  rocket,
  nastyRocket,
  inkDroplet,
  waterDroplet,
  heart
}

enum ActorCollisionType {
  none,
  collideAny,
  collideCircle
}

enum ActorCollisionEffect {
  none,
  breakTile,
  breakTileNasty,
  hurtCircle,
  healCircle,
  tileDirty,
  tileClean
}

enum ActorActionPhase {
  appearing,
  moving,
  disappearing
}